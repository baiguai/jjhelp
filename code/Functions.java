import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.Random;
import java.util.UUID;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.IOException;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.text.SimpleDateFormat;

/*
    GENERAL HELPER FUNCTIONS
    ----------------------------------------------------------------------------
    Methods designed to make life easier.
    ----------------------------------------------------------------------------
*/
public class Functions
{
    private static boolean Debugging = true;

    // Debuggig message
    public static void Debug(String file, String message)
    {
        if (Debugging)
        {
            System.out.println("\nDebug (" + file + "):\n - " + message + "\n--------\n");
        }
    }
    public static void Debug(String file, List<String> messages)
    {
        if (Debugging)
        {
            System.out.println("\nDebug (" + file + "):\n");
            for (String s : messages)
            {
                System.out.println(" - " + s + "\n");
            }
            System.out.println("--------\n");
        }
    }

    // Cleanup ID
    public static String IDCleanup(String id)
    {
        id = id.replace("[", "").replace("]", "").trim();
        return id;
    }

    // GUID Generator
    public static String GetGUID()
    {
        String output = "";

        UUID uuid = UUID.randomUUID();
        output = uuid.toString();

        return output;
    }

    public static String WrapLines(String input)
    {
        String[] arr = input.split("\n");
        String[] temp = null;
        List<String> output = new ArrayList<String>();
        String outputString = "";
        String line = "";
        int start = 0;
        int wrapPos = Integer.parseInt(GetSetting("Config/Theme.config", "line_width", "-1"));

        if (wrapPos <= 0) return input;
        
        for (String s : arr)
        {
            line = "";
            temp = s.split(" ");

            for (String l : temp)
            {
                if (!line.equals("")) line += " ";

                if ((line + l).length() > wrapPos)
                {
                    output.add(line.trim() + "\n");
                    line = l;
                }
                else
                {
                    line += l;
                }
            }

            if (!line.equals(""))
            {
                output.add(line.trim() + "\n");
            }
        } 

        for (String s : output)
        {
            outputString += s;
        }

        return outputString;
    }

    // Case insensitive match - or contains.
    public static boolean Match(String source1, String source2)
    {
        boolean isMatch = false;
        source2 = source2.replace("|", ",");
        String[] arr = source2.split(",");

        for (String itm: arr)
        {
            if (source1.equalsIgnoreCase(itm) ||
                source1.toLowerCase().contains(source2.toLowerCase()))
            {
                isMatch = true;
                break;
            }
        }

        return isMatch;
    }

    // Random Int
    // if min = -1, only use the max value
    public static int RandomInt(int min, int max)
    {
        Random rnd = new Random();
        int output = -1;
        if (min >= max)
        {
            max = min;
            min = -1;
        }
        if (min < 0) min = 0;

        while (output < min)
        {
            output = rnd.nextInt(max + 1);
        }
        
        return output;
    }

    public static void OutputRaw(String message)
    {
        System.out.println(message);
    }
    public static void Output(List<String> message)
    {
        System.out.println("\n\n");
        for (String s : message)
        {
            System.out.println(s);
        }
        System.out.println("\n\n");
    }
    public static void Output(String message)
    {
        System.out.println("\n\n" + message + "\n\n");
    }

    // Convert to Table name
    public static String ToTable(String name)
    {
        name = name.trim().toLowerCase();
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    // Array to string.
    public static String ArrayToString(String[] array, int startIndex, String delimiter)
    {
        String output = "";

        for (int ix = startIndex; ix < array.length; ix++)
        {
            if (!output.equals("")) output += delimiter;
            output += array[ix];
        }

        return output;
    }

    // Sql Cleanup.
    public static String SqlCleanup(String input)
    {
        if (input.equals("")) return input;

        input = input.replaceAll("'", "''");

        return input;
    }

    // Clear the console
    public static void ClearConsole()
    {
        try
        {
            final String os = System.getProperty("os.name");

            if (os.contains("Windows"))
            {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            }
            else
            {
                System.out.print("\033[H\033[2J");
            }
        }
        catch (final Exception e)
        {
            //  Handle any exceptions.
        }
    }

    // Get Setting
    /*
        -   file
            This parameter is the file path without extension.
            It is relative to the application root.
            It must have a .properties extension.
    */
    public static String GetSetting(String file, String key, String defVal)
    {
        Properties prop = new Properties();
        String value = "";

        try
        {
            Properties props = new Properties();
            String propFile = file;
            FileInputStream inputStream = new FileInputStream(propFile);

            if (inputStream != null)
            {
                prop.load(inputStream);
                value = prop.getProperty(key);
            }
            else
            {
                key = "";
                //throw new FileNotFoundException("Config file not found: " + propFile);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        if (value.equals("")) value = defVal;

        return value;
    }


    // Get Formatted Date
    public static String GetDate()
    {
        String output = "";
        
        Date today = new Date();
        SimpleDateFormat TS_FORMAT = new SimpleDateFormat("yyyy.MM.dd_HH.mm.ss");
        output = TS_FORMAT.format(today);

        return output;
    }



    public static List<String> ListFiles(String path, String extension)
    {
        List<String> files = new ArrayList<String>();

        try
        {
            // Iterate through the plugins dir
            File[] fileList = new File(path).listFiles();

            if (fileList == null) return files;

            for (File file : fileList)
            {
                if (!file.isDirectory() && file.getName().indexOf(extension) >= 0)
                {
                    files.add(file.getName());
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return files;
    }

    public static List<String> ListFiles(String path)
    {
        List<String> files = new ArrayList<String>();

        try
        {
            // Iterate through the plugins dir
            File[] fileList = new File(path).listFiles();

            if (fileList == null) return files;

            for (File file : fileList)
            {
                if (!file.isDirectory())
                {
                    files.add(file.getName());
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return files;
    }

    public static List<String> ListDirs(String path)
    {
        List<String> files = new ArrayList<String>();

        try
        {
            // Iterate through the plugins dir
            File[] fileList = new File(path).listFiles();

            if (fileList == null) return files;

            for (File file : fileList)
            {
                if (file.isDirectory())
                {
                    files.add(file.getName());
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return files;
    }

    public static List<String> ListJarFiles(String jar, String extension)
    {
        List<String> files = new ArrayList<String>();

        try
        {
            JarFile jarFile = new JarFile(jar);
            Enumeration allEntries = jarFile.entries();

            while (allEntries.hasMoreElements()) {
                JarEntry entry = (JarEntry) allEntries.nextElement();
                String name = entry.getName();

                if (name.indexOf(extension) >= 0)
                {
                    files.add(name);
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return files;
    }

    public static List<String> ReadFile(String file)
    {
        List<String> inp = new ArrayList<String>();
        String line;

        try
        {
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                while ((line = br.readLine()) != null) {
                    inp.add(line);
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return inp;
    }

    public static void WriteFile(String filePath, String content)
    {
        try {
            File file = new File(filePath);
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(content);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void OverwriteToFile(String file, List<String> contents)
    {
        File f = new File(file);
        if (f.exists())
        {
            f.delete();
        }

        try
        {
            PrintWriter writer = new PrintWriter(file, "UTF-8");

            for (String s : contents)
            {
                writer.println(s);
            }

            writer.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            Functions.Output("File: " + file);
        }
    }

    public static Document GetDoc(String file)
    {
        Document doc = null;
        File xmlFile = null;
        DocumentBuilderFactory dbFactory = null;
        DocumentBuilder dBuilder = null;

        try
        {
            xmlFile = new File(file);
            dbFactory = DocumentBuilderFactory.newInstance();
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(xmlFile);

            doc.getDocumentElement().normalize();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return doc;
    }


    // Copy Directories
    public static void CopyFileOrFolder(File source, File dest) throws IOException
    {
        if (source.isDirectory())
            copyFolder(source, dest);
        else {
            ensureParentFolder(dest);
            copyFile(source, dest);
        }
    }

    private static void copyFolder(File source, File dest) throws IOException
    {
        if (!dest.exists())
            dest.mkdirs();
        File[] contents = source.listFiles();
        if (contents != null) {
            for (File f : contents) {
                File newFile = new File(dest.getAbsolutePath() + File.separator + f.getName());
                if (f.isDirectory())
                    copyFolder(f, newFile);
                else
                    copyFile(f, newFile);
            }
        }
    }

    private static void copyFile(File source, File dest) throws IOException
    {
        Files.copy(source.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    private static void ensureParentFolder(File file) {
        File parent = file.getParentFile();
        if (parent != null && !parent.exists())
            parent.mkdirs();
    } 
}
