import java.util.*;
import java.awt.Color;

public class Markdown
{
    /* PROPERTIES */
    //
        private boolean DEBUG = false;

        private boolean openIgnoreMD = false;
        private boolean openBoldBlk = false;
        private boolean openItalicBlk = false;
        private boolean openUnderlineBlk = false;
        private boolean openOrderedLst = false;
        private boolean openOrderedLstItm = false;
        private boolean openUnorderedLst = false;
        private boolean openUnorderedLstItm = false;
        private int currentIndent = 0;
        private int indentStep = 0;
        private boolean openBlockQt = false;
        private boolean openCode = false;
        private boolean openTable = false;

        private List<TableColumn> columns = null;
            public List<TableColumn> GetColumns() { return columns; }
            public void SetColumns(List<TableColumn> val) { columns = val; }
            public void AddColumn(TableColumn val) { if (columns == null) return; columns.add(val); }
            public void ClearColumns() { columns = null; }
    //


    public String MarkDownRow(String row)
    {
        boolean doBreak = true;
        boolean handled = false;
        String tmp = "";

        row = row.replace("\"", "&#34;");

        if (!openIgnoreMD && row.toLowerCase().indexOf("#ignore#") >= 0)
        {
            openIgnoreMD = true;
            return CloseAll() + "<pre>";
        }
        if (openIgnoreMD && row.toLowerCase().indexOf("#ignore#") >= 0)
        {
            openIgnoreMD = false;
            return "</pre>";
        }

        if (openIgnoreMD)
        {
            return row + "<br />";
        }

        // If the row is blank close any open lists
        if (row.trim().equals("") && !openCode)
        {
            row = CloseLists(row);
            row = row += "<br />";
            doBreak = false;
        }

        // HEADINGS
        if (row.length() > 6 && !openCode && row.indexOf("######") == 0)
        {
            doBreak = false;
            handled = true;
            row = CloseLists(row);
            row = "<h6>" + row.replace("######", "") + "</h6>";
        }
        if (row.length() > 5 && !openCode && row.indexOf("#####") == 0)
        {
            doBreak = false;
            handled = true;
            row = CloseLists(row);
            row = "<h5>" + row.replace("#####", "") + "</h5>";
        }
        if (row.length() > 4 && !openCode && row.indexOf("####") == 0)
        {
            doBreak = false;
            handled = true;
            row = CloseLists(row);
            row = "<h4>" + row.replace("####", "") + "</h4>";
        }
        if (row.length() > 3 && !openCode && row.indexOf("###") == 0)
        {
            doBreak = false;
            handled = true;
            row = CloseLists(row);
            row = "<h3>" + row.replace("###", "") + "</h3>";
        }
        if (row.length() > 2 && !openCode && row.indexOf("##") == 0)
        {
            doBreak = false;
            handled = true;
            row = CloseLists(row);
            row = "<h2>" + row.replace("##", "") + "</h2>";
        }
        if (row.length() > 1 && !openCode && row.indexOf("#") == 0)
        {
            doBreak = false;
            handled = true;
            row = CloseLists(row);
            row = "<h1>" + row.replace("#", "") + "</h1>";
        }

        // TABS (3 spaces)
        if (row.length() > 3 && !openCode)
        {
            doBreak = false;
            row = row.replace("...", "&nbsp;&nbsp;&nbsp;");
        }

        // BLOCKQUOTES
        if (!handled && !openCode && row.length() > 2 && row.trim().indexOf("> ") == 0)
        {
            handled = true;
            row = row.replace("> ", "");
            row = CloseLists(row);
            if (!openBlockQt)
            {
                openBlockQt = true;
                row = "<blockquote> " + row;
            }
        }
        if (!handled && openBlockQt && (row.length() < 2 || row.trim().indexOf("> ") < 0))
        {
            row = "</blockquote> " + row;
            openBlockQt = false;
        }

        // ORDERED LISTS
        if (!handled && !openCode && row.length() > 2 && (row.trim().indexOf(">#") == 0 || row.trim().indexOf("1. ") == 0))
        {
            handled = true;
            int tmpIndent = GetIndent(row);
            int tmpI = 0;

            row = row.replaceFirst(">#", "").replaceFirst("1. ", "");

            if (!openOrderedLst)
            {
                row = CloseLists(row);
                // doBreak = false;
                openOrderedLst = true;
                openOrderedLstItm = true;
                row = "<ol> <li> " + row;
            }
            else
            {
                row = "<li> " + row;
                if (openOrderedLstItm) row = "</li> " + row;
                openOrderedLstItm = true;

                if (tmpIndent > currentIndent)
                {
                    row = "<ol> " + row;
                }
                if (tmpIndent < currentIndent)
                {
                    tmpI = currentIndent;

                    if (openOrderedLstItm) row = "</li></ol>" + row;
                    else row = "</ol>" + row;

                    tmpI = tmpI - indentStep;

                    while (tmpI > currentIndent)
                    {
                        row = "</ol>" + row;
                        tmpI = (tmpI - indentStep);
                    }
                }

                currentIndent = tmpIndent;
            }
        }

        // UNORDERED LISTS
        if (!handled && !openCode && row.length() > 2 && (row.trim().indexOf(">.") == 0 || row.trim().indexOf("* ") == 0))
        {
            handled = true;
            int tmpIndent = GetIndent(row);
            int tmpI = 0;

            row = row.replaceFirst(">.", "").replaceFirst("\\* ", "");

            if (!openUnorderedLst)
            {
                row = CloseLists(row);
                // doBreak = false;
                openUnorderedLst = true;
                openUnorderedLstItm = true;
                row = "<ul> <li> " + row;
            }
            else
            {
                row = "<li> " + row;
                if (openUnorderedLstItm) row = "</li> " + row;
                openUnorderedLstItm = true;

                if (tmpIndent > currentIndent)
                {
                    row = "<ul> " + row;
                }
                if (tmpIndent < currentIndent)
                {
                    tmpI = currentIndent;

                    if (openOrderedLstItm) row = "</li></ul>" + row;
                    else row = "</ul>" + row;

                    tmpI = tmpI - indentStep;

                    while (tmpI > currentIndent)
                    {
                        row = "</ul>" + row;
                        tmpI = (tmpI - indentStep);
                    }
                }

                currentIndent = tmpIndent;
            }
        }

        // BOLD
        if (row.length() > 2)
        {
            while (row.indexOf("**") >= 0)
            {
                if (!openBoldBlk)
                {
                    row = row.replaceFirst("\\*\\*", "<strong>");
                    openBoldBlk = true;
                }
                else
                {
                    row = row.replaceFirst("\\*\\*", "</strong>");
                    openBoldBlk = false;
                }
            }
        }

        // ITALIC
        if (row.length() > 1)
        {
            while (row.indexOf("*") >= 0)
            {
                if (!openItalicBlk)
                {
                    row = row.replaceFirst("\\*", "<em>");
                    openItalicBlk = true;
                }
                else
                {
                    row = row.replaceFirst("\\*", "</em>");
                    openItalicBlk = false;
                }
            }
        }

        // COLOR
        if (row.length() > 8)
        {
            String clr = "";
            Color c = null;
            int clrStart = 0;

            while (row.indexOf("^#") >= 0)
            {
                clrStart = row.indexOf("^#");

                if (clrStart >= 0)
                {
                    clrStart = clrStart + 1;

                    if (row.length() > clrStart + 8)
                    {
                        clr = row.substring(clrStart, clrStart + 7);
                        row = row.replace("^" + clr, "<font style='color: " + clr + "'>");
                    }
                }
            }

            while (row.indexOf("^") >= 0)
            {
                row = row.replace("^", "</font>");
            }
        }

        // HR
        if (!handled && !openTable && GetColumns() == null && row.length() >= 3 && row.indexOf("---") >= 0)
        {
            row = CloseLists(row);
            row = row.replace("---", " <hr /> ");
        }

        // CODE
        if (!handled && openCode)
        {
            row = row.replace("<", "&lt;").replace(">", "&gt;").replace(" ", "&nbsp;"); //  + "<br />";
        }
        if (!handled && ((row.length() >= 3 && row.indexOf("```") >= 0) || (row.length() >= 2 && row.indexOf("``") >= 0)))
        {
            handled = true;
            doBreak = false;
            row = row.replace("```", "");
            row = row.replace("``", "");
            if (!openCode)
            {
                row = CloseLists(row);
                openCode = true;
                row = "<br /><code> " + row;
            }
            else
            {
                openCode = false;
                row = "</code> " + row;
            }
        }

        // IMAGES
        if (row.length() > 1 && row.indexOf("![") >= 0)
        {
            String[] arr = null;
            String[] arr2 = null;
            boolean newWin = false;

            int altStart = 0;
            int altEnd = 0;
            int lnkStart = 0;
            int lnkEnd = 0;
            String alt = "";
            String lnk = "";
            String imgw = "";
            String align = "";
            String alignWrpOpen = "";
            String alignWrpClose = "";
            String margin = "";
            String style = "";
            String attribs = "";
            tmp = "";

            while (row.indexOf("![") >= 0)
            {
                alt = "";
                lnk = "";
                align = "";
                tmp = row;

                // Handle the alignment
                if (tmp.indexOf("--]") >= 0) align = "right";
                if (tmp.indexOf("[--") >= 0 && align.equals("right")) align = "middle";
                if (tmp.indexOf("[--") >= 0 && !align.equals("middle")) align = "left";
                if (tmp.indexOf("[|") >= 0) align = "floatleft";
                if (tmp.indexOf("|]") >= 0) align = "floatright";

                if (align.equals("right"))
                {
                    alignWrpOpen = "<div style='text-align: right;'>";
                    alignWrpClose = "</div>";
                }
                if (align.equals("middle"))
                {
                    alignWrpOpen = "<div style='text-align: center;'>";
                    alignWrpClose = "</div>";
                }

                margin = "10px";

                // Clean out the alignment characters - so the replacement is predictable
                row = row.replace("[--", "[").replace("--]", "]").replace("[|", "[").replace("|]", "]");

                tmp = row;

                altStart = tmp.indexOf("[");

                if (altStart < 0) break;

                tmp = row.substring(altStart + 1);
                altEnd = tmp.indexOf("]");

                if (altEnd < 0) break;

                alt = tmp.substring(0, altEnd);

                lnkStart = tmp.indexOf("(");

                if (lnkStart < 0) break;

                tmp = tmp.substring(lnkStart + 1);

                lnkEnd = tmp.indexOf(")");

                if (lnkEnd < 0) break;

                lnk = tmp.substring(0, lnkEnd);
                arr = lnk.split(",");
                if (arr.length == 2)
                {
                    lnk = arr[0].trim();
                    imgw = arr[1];
                }

                // build the style value
                if (!margin.equals(""))
                {
                    if (!style.equals("")) style += "; ";
                    style += "margin: " + margin;
                }
                if (!imgw.equals(""))
                {
                    if (!style.equals("")) style += "; ";
                    style += "width: " + imgw;
                }
                if (align.equals("floatleft"))
                {
                    if (!style.equals("")) style += "; ";
                    style += "float: left";
                }
                if (align.equals("floatright"))
                {
                    if (!style.equals("")) style += "; ";
                    style += "float: right";
                }

                if (!style.equals(""))
                {
                    style = " style=\"" + style + ";\" ";
                }

                // build the attributes value
                if (align.equals("right"))
                {
                    attribs = " align=\"right\" ";
                }
                if (align.equals("middle"))
                {
                    attribs = " align=\"middle\" ";
                }

                row = row.replace("![" + alt + "](" + lnk + "," + imgw + ")", alignWrpOpen + "<img alt=\"" + alt.trim() + "\" src=\"" + lnk.trim() + "\" " + style + attribs + " />" + alignWrpClose);
            }
        }

        // LINKS
        if (row.length() > 1 && row.indexOf("[") >= 0)
        {
            String[] arr = null;
            String[] arr2 = null;
            boolean newWin = false;

            int lblStart = 0;
            int lblEnd = 0;
            int lnkStart = 0;
            int lnkEnd = 0;
            String lbl = "";
            String lnk = "";
            String win = "";
            boolean lnkTopic = false;
            String bkTopic = "";
            tmp = "";

            while (row.indexOf("](") >= 0)
            {
                lbl = "";
                lnk = "";
                win = "";
                tmp = row;
                lblStart = tmp.indexOf("[");

                if (lblStart < 0) break;

                tmp = row.substring(lblStart + 1);
                lblEnd = tmp.indexOf("]");

                if (lblEnd < 0) break;

                lbl = tmp.substring(0, lblEnd);

                if (tmp.indexOf("(!") >= 0) win = " target=\"_blank\"";
                if (tmp.indexOf("(@") >= 0) lnkTopic = true;

                lnkStart = tmp.indexOf("(");

                if (lnkStart < 0) break;

                tmp = tmp.substring(lnkStart + 1);

                lnkEnd = tmp.indexOf(")");

                if (lnkEnd < 0) break;

                lnk = tmp.substring(0, lnkEnd).replace("!", "").replace("@", "");

                if (lnkTopic)
                {
                    arr2 = lnk.split(",");
                    if (arr2.length == 2)
                    {
                        lnk = arr2[0].trim();
                        bkTopic = arr2[1].trim();
                        row = row.replace("[" + lbl + "](@" + lnk + ", " + bkTopic + ")", "<a href=\"javascript:linkTopic('" + lnk + "', '" + bkTopic + "');\">" + lbl + "</a>");
                        row = row.replace("[" + lbl + "](@" + lnk + "," + bkTopic + ")", "<a href=\"javascript:linkTopic('" + lnk + "', '" + bkTopic + "');\">" + lbl + "</a>");
                    }
                    else
                    {
                        row = row.replace("[" + lbl + "](@" + lnk + ")", "<a href=\"javascript:linkTopic('" + lnk + "');\">" + lbl + "</a>");
                    }
                }
                else
                {
                    if (!win.equals(""))
                    {
                        row = row.replace("[" + lbl + "](!" + lnk + ")", "<a href=\"" + lnk + "\" " + win + ">" + lbl + "</a>");
                    }
                    else
                    {
                        row = row.replace("[" + lbl + "](" + lnk + ")", "<a href=\"" + lnk + "\">" + lbl + "</a>");
                    }
                }
            }
        }

        // TABLES
        if (row.length() > 2 && !openCode && row.trim().indexOf("|") == 0)
        {
            row = row.substring(1, row.length()-1);

            // Beginning a table
            if (!openTable && GetColumns() == null)
            {
                SetColumns(new ArrayList<TableColumn>());
                TableColumn col = null;
                row = row.replace("\\|", "$$"); // Handle escaped pipes
                row = row.replace("|", "@@");
                String[] titles = row.split("@@");
                for (String c : titles)
                {
                    if (c.trim().equals("")) continue;

                    col = new TableColumn();
                    col.SetTitle(c.trim().replace("$$", "|"));
                    AddColumn(col);
                }
                // Clear the row
                return "";
            }
            // Define the columns settings
            if (!openTable && GetColumns() != null)
            {
                row = row.replace("|", "@@");
                String[] titles = row.split("@@");
                int ix = 0;

                row = "<table cellspacing='0' class='content_table'><tr>";

                for (String c : titles)
                {
                    if (ix > GetColumns().size() -1) break;
                    tmp = c.trim();
                    String align = "";
                    String width = "";

                    if (tmp.length() > 2)
                    {
                        // Define alignment
                        if (tmp.substring(tmp.length()-1).equals(":"))
                        {
                            align = "right";
                            if (tmp.substring(0, 1).equals(":"))
                            {
                                align = "center";
                            }
                        }

                        GetColumns().get(ix).SetAlign(align);

                        tmp = tmp.replace(":", "").replace("-", "");

                        // Define width
                        if (tmp.length() > 0)
                        {
                            width = tmp;
                        }

                        GetColumns().get(ix).SetWidth(width);
                    }

                    row += "<th>" + GetColumns().get(ix).GetTitle() + "</th>";

                    ix++;
                }

                row += "</tr>";
                openTable = true;
                return row;
            }
            if (openTable && GetColumns() != null)
            {
                row = row.replace("\\|", "$$");
                row = row.replace("|", "@@");
                String[] cells = row.split("@@");
                row = "<tr>";
                int ix = 0;

                for (String c : cells)
                {
                    if (c.trim().equals("")) continue;

                    row += "<td";
                    if (ix < GetColumns().size())
                    {
                        if (!GetColumns().get(ix).GetAlign().equals("") || !GetColumns().get(ix).GetWidth().equals(""))
                        {
                            row += " style='";
                            if (!GetColumns().get(ix).GetAlign().equals("")) row += "text-align: " + GetColumns().get(ix).GetAlign() + ";";
                            if (!GetColumns().get(ix).GetWidth().equals("")) row += "width: " + GetColumns().get(ix).GetWidth() + ";";
                            row += "'";
                        }
                    }
                    row += ">";
                    row += c.replace("$$", "|") + "</td>";

                    ix++;
                }
                row += "</tr>";
                return row;
            }
        }
        else
        {
            if (openTable)
            {
                row = "</table> " + row;
                ClearColumns();
                openTable = false;
            }
        }

        if (doBreak) row += "<br />";

        row = row.replace("\"", "\\\"") + " ";

        if (DEBUG) System.out.println(row);

        return row;
    }



    public int GetIndent(String row)
    {
        int spaceCount = 0;

        for (char c : row.toCharArray()) {
            if (c == ' ') {
                 spaceCount++;
            }
            else break;
        }

        // Set the indent step
        if (currentIndent == 0) indentStep = spaceCount;

        // Reset the indent step
        if (spaceCount == 0) indentStep = 0;

        return spaceCount;
    }


    public void ClearBooleans()
    {
        openBoldBlk = false;
        openItalicBlk = false;
        openUnderlineBlk = false;
        openOrderedLst = false;
        openUnorderedLst = false;
        openBlockQt = false;
        openCode = false;
    }

    public String CloseLists(String row)
    {
        if (openOrderedLstItm || openUnorderedLstItm)
        {
            if (openOrderedLstItm) openOrderedLstItm = false;
            if (openUnorderedLstItm) openUnorderedLstItm = false;
            row = CloseListItems(row);
        }

        row = CloseOL(row);
        row = CloseUL(row);
        return row;
    }

    private String CloseListItems(String row)
    {
        row = CloseOLLI(row);
        row = CloseULLI(row);
        return row;
    }

    private String CloseOL(String row)
    {
        if (openOrderedLst) row = row + " </ol>";
        openOrderedLst = false;
        row = CloseOLLI(row);
        return row;
    }

    private String CloseOLLI(String row)
    {
        if (openOrderedLstItm) row = row + " </li>";
        openOrderedLstItm = false;
        return row;
    }

    private String CloseUL(String row)
    {
        if (openUnorderedLst) row = row + " </ul>";
        openUnorderedLst = false;
        row = CloseULLI(row);
        return row;
    }

    private String CloseULLI(String row)
    {
        if (openUnorderedLstItm) row = row + " </li>";
        openUnorderedLstItm = false;
        return row;
    }

    private String CloseAll()
    {
        String output = "";

        if (openBlockQt) output = "</blockquote>" + output;
        if (openCode) output = "</code>" + output;
        output = CloseLists("");
        if (openBoldBlk) output = "</strong>" + output;
        if (openItalicBlk) output = "</em>" + output;


        return output;
    }
}
