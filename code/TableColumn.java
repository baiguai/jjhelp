import java.util.*;

public class TableColumn
{
    /* PROPERTIES */
    //
        private String title = "";
            public String GetTitle() { return title; }
            public void SetTitle(String val) { title = val; }

        private String align = "";
            public String GetAlign() { return align; }
            public void SetAlign(String val) { align = val; }

        private String width = "";
            public String GetWidth() { return width; }
            public void SetWidth(String val) { width = val; }
    //
}
