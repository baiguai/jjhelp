import java.io.*;
import java.io.Console;
import java.util.*;
import java.util.Scanner;
import java.awt.Desktop;

public class Parser
{
    /* PROPERTIES */
    //
        private boolean DEBUG = true;

        private String Delim = "@@";
        private String GlobTag = "%%";
        private String sourceDir = "";
        private List<List<String>> TOC = new ArrayList<List<String>>();
        private int TOCIndex = 0;
        private String ThemeRoot = "config/templates/";

        private Markdown markdown = null;
            public Markdown GetMarkdown() { if (markdown == null) markdown = new Markdown(); return markdown; }
    //

    public void BuildHelp(String macroIn)
    {
        String input = "";
        Scanner scan = new Scanner(System.in);
        String title = "";
        String titleTag = "";
        String outputDir = "";
        String template = "standard";
        File f = null;
        String path = "";

        // If there is a macro passed in, build that
        if (!macroIn.equals(""))
        {
            input = "1";
        }

        // Set the Root DIR
        try
        {
            // Actions
            if (macroIn.equals(""))
            {
                input = "";
                Functions.Output("1 - Build via macro\n2 - Specify title / source / output / theme");
                System.out.print(">> ");
                input = scan.nextLine();

                if (input.equals("exit")) return;
            }

            if (input.equals("1"))
            {
                if (macroIn.equals(""))
                {
                    input = "";
                    Functions.Output("Enter the macro name (macro file)");
                    System.out.print(">> ");
                    input = scan.nextLine();
                }
                else
                {
                    input = macroIn;
                }

                path = "config/macros/" + input;

                f = new File(path);
                if (input.equals("") || !f.exists())
                {
                    System.out.println("Macro file not found. Acceptable values are:");
                    List<String> macros = Functions.ListFiles("config/macros");
                    String ms = "";
                    for (String m : macros)
                    {
                        if (!ms.equals("")) ms += "\n";
                        ms += m;
                    }
                    System.out.println(ms);

                    return;
                }

                List<String> macro = Functions.ReadFile("config/macros/" + input);
                if (macro.size() == 4)
                {
                    title = macro.get(0).trim();
                    titleTag = macro.get(0).trim();
                    sourceDir = macro.get(1).trim();
                    outputDir = macro.get(2).trim();
                    template = macro.get(3).trim();

                    f = new File(sourceDir + "/Images/title.png");
                    if (f.exists())
                    {
                        title = "<img src=\"Images/title.png\" alt=\"" + title + "\" />";
                    }
                }
            }
            else
            {
                // Title
                input = "";
                Functions.Output("Enter your help system's title:");
                System.out.print(">> ");
                input = scan.nextLine();

                if (input.equals("exit")) return;

                if (!input.equals(""))
                {
                    title = input;
                    titleTag = title;
                }
                else
                {
                    Functions.Output("ERROR! Be sure to specify the title of your help system.");
                    return;
                }

                // Source Dir
                input = "";
                Functions.Output("Enter your source file directory (No trailing / ) - relative to this .jar\nTypically definitions/<help system>:");
                System.out.print(">> ");
                input = scan.nextLine();

                if (input.equals("exit")) return;

                if (!input.equals("")) sourceDir = input;
                else
                {
                    Functions.Output("ERROR! Be sure to specify the directory that contains your source files (No trailing / ).");
                    return;
                }

                // Output Dir
                input = "";
                Functions.Output("Enter the output directory - relative to this .jar\nTypically output/<help system>:");
                System.out.print(">> ");
                input = scan.nextLine();

                if (input.equals("exit")) return;

                if (!input.equals("")) outputDir = input;
                else
                {
                    Functions.Output("ERROR! Be sure to specify the output directory.");
                    return;
                }

                // Theme
                input = "";

                List<String> thms = Functions.ListDirs("config/templates");
                String themes = "";
                for (String t : thms)
                {
                    if (!themes.equals("")) themes += "\n";
                    themes += t;
                }

                Functions.Output("Enter the theme. Possible values:\n" + themes);
                System.out.print(">> ");
                input = scan.nextLine();

                if (input.equals("exit")) return;

                if (!input.equals("")) template = input;
                else
                {
                    Functions.Output("ERROR! Be sure to specify the theme.");
                    return;
                }
            }
        }
        catch (Exception ex)
        {
            //ex.printStackTrace();
        }

        Functions.Output("Building Help...");


        // Copy the template directory as the output directory
        try
        {
            File srcF = new File(ThemeRoot + template);
            File destF = new File(outputDir);
            Functions.CopyFileOrFolder(srcF, destF);

            srcF = new File(sourceDir + "/Images");
            destF = new File(outputDir + "/Images");

            if (srcF.exists())
            {
                Functions.CopyFileOrFolder(srcF, destF);
            }

            srcF = new File(sourceDir + "/Files");
            destF = new File(outputDir + "/Files");

            if (srcF.exists())
            {
                Functions.CopyFileOrFolder(srcF, destF);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        // Set the title
        String glob = outputDir + "/Data/Global.js";
        List<String> tempDat = Functions.ReadFile(glob);
        List<String> tempOut = new ArrayList<String>();
        for (String d : tempDat)
        {
            d = d.replace("@@title@@", title);
            d = d.replace("@@titletag@@", titleTag);
            tempOut.add(d);
        }
        Functions.OverwriteToFile(glob, tempOut);

        glob = outputDir + "/index.htm";
        tempDat = Functions.ReadFile(glob);
        tempOut = new ArrayList<String>();
        for (String d : tempDat)
        {
            d = d.replace("@@title@@", title);
            d = d.replace("@@titletag@@", titleTag);
            tempOut.add(d);
        }
        Functions.OverwriteToFile(glob, tempOut);


        // Build the TOC
        String toc = BuildToc(sourceDir + "/");
        String topics = "";

        if (!toc.equals(""))
        {
            toc = "var TreeData = " + toc;
            topics = BuildTopics(sourceDir + "/");
        }

        // Build the Data
        Functions.WriteFile(outputDir + "/Data/Data.js", toc + "\n\n" + topics);

        Functions.OutputRaw("Done.");

        try
        {
            f = new File(outputDir + "/index.htm");

            if (f.exists()) 
            {
                if (Desktop.isDesktopSupported()) 
                {
                    Desktop.getDesktop().open(f);
                }
                else
                {
                    System.out.println("File does not exists!");
                }
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    // Build the TOC json
    private String BuildToc(String rootPath)
    {
        String toc = "";
        List<String> file = Functions.ReadFile(rootPath + "toc.txt");

        for (int ix = 0; ix < file.size(); ix++)
        {
            String node = BuildTocItem(0, ix, file);
            if (!node.equals(""))
            {
                toc += "[" + node + "]";
            }
        }

        return toc;
    }
    private String BuildTocItem(int indent, int rowIndex, List<String> contents)
    {
        /*
           var TreeData = [{"name":"World Weaver API Reference","id":"Node_70c30ce04454b86212980458249da0a6","is_open":true,"children":[{"name":"Welcome to Genesrc","id":"06b3d0d4d25a3554bbb223e302c5959d"}]}]

            [
                {"name":"World Weaver API Reference","id":"Node_70c30ce04454b86212980458249da0a6","is_open":false,
                    "children":[
                        {"name":"Welcome to Genesrc","id":"06b3d0d4d25a3554bbb223e302c5959d"}
                    ]
                }
            ]
        */
        String toc = "";
        List<String> row = null;
        int nextIndent = indent;

        for (int ix = rowIndex; ix < contents.size(); ix++)
        {
            String children = "";
            String itm = contents.get(ix);

            if (GetIndent(itm) < indent)
            {
                break;
            }

            if (GetIndent(itm) == indent)
            {
                String[] arr = itm.split(Delim);

                if (arr.length >= 1)
                {
                    if (GetIndent(itm) == indent)
                    {
                        String guid = Functions.GetGUID();
                        TOCIndex = ix;

                        if (arr.length > 1)
                        {
                            // Add to the TOC
                            List<String> tocItem = new ArrayList<String>();
                            tocItem.add(guid);
                            tocItem.add(arr[1].trim());
                            TOC.add(tocItem);
                        }

                        // Build the JSON
                        toc += "{\"name\":\"" + Encode(arr[0]).trim() + "\",";
                        toc += "\"id\":\"" + guid + "\",";
                        toc += "\"is_open\":false";

                        if (ix < contents.size() - 1)
                        {
                            nextIndent = GetIndent(contents.get(ix+1));
                            if (nextIndent > indent)
                            {
                                children = BuildTocItem(nextIndent, ix+1, contents);
                                if (!children.equals(""))
                                {
                                    toc += ",\"children\":[" + children + "]";
                                }
                            }
                        }

                        toc += "},";
                    }
                }
            }
        }

        if (toc.length() > 0) toc = toc.substring(0, toc.length()-1);

        return toc;
    }

    private String BuildTopics(String rootPath)
    {
        /*
            searchDB[schix] = new Array();
            searchDB[schix]["ID"] =                 "06b3d0d4d25a3554bbb223e302c5959d";
            searchDB[schix]["Title"] =              "Welcome to Genesrc";
            searchDB[schix]["SubTitle"] =           "";
            searchDB[schix]["Url"] =                "";
            searchDB[schix]["Target"] =             "";
            searchDB[schix]["Description"] =        "";
            searchDB[schix]["Topic"] =              "<p>\n    <strong>Welcome</strong>\n    <br />\n    Genesrc is a collection of tips, articles, tutorials and references that make web development, application development, graphics, audio and OS operations easier. The entire collection has been built with a portable help system comprised solely of html and javascript. Genesrc is <em>self contained</em>, simply transport the genesrc directory or access it from a thumb drive. It is not dependent upon web servers or specific operating systems. To run genesrc all you need is a fairly modern web browser.\n</p>";
            searchDB[schix]["Keywords"] =           "";
            searchDB[schix]["Categories"] =         "news";
            searchDB[schix]["Status"] =             "";
            searchDB[schix]["Alias"] =              "";
            searchRef[searchDB[schix]["ID"]] = searchDB[schix]["Url"]; schix ++;
        */
        String topics = "";

        for (List<String> itm : TOC)
        {
            GetMarkdown().ClearBooleans();
            List<String> contents = Functions.ReadFile(rootPath + itm.get(1));
            String topicText = "";

            String subtitle = "";
            String url = "";
            String target = "";
            String description = "";
            String topic = "";
            String keywords = "";
            String categories = "";
            String status = "";
            String alias = "";

            topics += "searchDB[schix] = new Array();\n";
            topics += "searchDB[schix][\"ID\"] = \"" + itm.get(0) + "\";\n";

            for (int ix = 0; ix < contents.size(); ix++)
            {
                String row = Encode(contents.get(ix));
                row = HandleGlobal(sourceDir, row);
                String[] rowArr = row.split(":");

                if (ix == 0)
                {
                    topics += "searchDB[schix][\"Title\"] = \"" + row + "\";\n";
                }
                else
                {
                    if (rowArr.length > 1)
                    {
                        switch(rowArr[0].trim().toLowerCase())
                        {
                            case "keywords":
                                keywords = rowArr[1];
                                break;
                            case "categories":
                                categories = rowArr[1];
                                break;
                            case "status":
                                status = rowArr[1];
                                break;
                            case "alias":
                                alias = rowArr[1];
                                break;
                            default:
                                topicText += GetMarkdown().MarkDownRow(row);
                                break;
                        }
                    }
                    else
                    {
                        topicText += GetMarkdown().MarkDownRow(row);
                    }
                }
            }


            topicText = EndDoc(topicText);

            topics += "searchDB[schix][\"SubTitle\"] = \"" + subtitle + "\";\n";
            topics += "searchDB[schix][\"Url\"] = \"" + url + "\";\n";
            topics += "searchDB[schix][\"Target\"] = \"" + target + "\";\n";
            topics += "searchDB[schix][\"Description\"] = \"" + description + "\";\n";
            topics += "searchDB[schix][\"Topic\"] = \"" + topicText + "\";\n";
            topics += "searchDB[schix][\"Keywords\"] = \"" + keywords + "\";\n";
            topics += "searchDB[schix][\"Categories\"] = \"" + categories + "\";\n";
            topics += "searchDB[schix][\"Status\"] = \"" + status + "\";\n";
            topics += "searchDB[schix][\"Alias\"] = \"" + alias + "\";\n";
            topics += "searchRef[searchDB[schix][\"ID\"]] = searchDB[schix][\"Url\"]; schix ++;\n";
            topics += "\n";
        }

        return topics;
    }


    private String HandleGlobal(String srcDir, String output)
    {
        String g = srcDir + "/global.txt";
        File f = new File(g);
        if (!f.exists()) return output;
        List<String> data = Functions.ReadFile(g);
        String[] arr = null;

        for (String r : data)
        {
            if (r.equals("")) continue;

            arr = r.split("=", 2);
            if (arr.length == 2)
            {
                output = output.replaceAll(GlobTag + arr[0].trim() + GlobTag, arr[1]);
            }
        }

        return output;
    }




    // Helper Methods
    private String Encode(String input)
    {
        input = input.replace("\"", "\\\"");

        return input;
    }

    private String EndDoc(String content)
    {
        content = GetMarkdown().CloseLists(content);
        content = GetMarkdown().CloseLists(content);
        return content;
    }

    private int GetIndent(String row)
    {
        int output = 0;

        for (int i = 0; i < row.length(); i++)
        {
            if (row.charAt(i) == ' ')
            {
                output++;
            }
            else
            {
                break;
            }
        }

        return output;
    }
}
