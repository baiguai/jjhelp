Youtubedl Download Audio
alias:youtubedl_audio
keywords:youtubedl mp3 download mp3
### Open a console window
### cd to the desired directory
### Enter:
``
youtubedl --extract-audio --audio-format mp3 <youtube video url>
``

*Note:*

Be sure the version of youtubedl is current. [To upgrade youtubedl click here](@youtubedl_upgrade,youtubedl_audio).

