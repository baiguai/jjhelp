Creating the Server

## Creating the file
``
touch server.js
``

## Server Contents
Add the following code to the new server.js file using your preferred editor.

``
const express = require('express');
const port = process.env.PORT || 3000;

const app = express();

// Add the routes
require('./routes/htmlroutes') ( app );

app.listen(port, () => {
    console.log('App listening on port: ' + port);
});
``
