VIMRC For Writing Example
### Defining a VIMRC for Writing
Below is an example of a VIMRC designed for *general writing* as opposed to *development*:

``
"-- GENERAL SETTINGS --
set guifont=Consolas:h9:cANSI
set gfn=Consolas\ 11
set guioptions-=T
set nonumber
set wrap
set linebreak
set stal=0
set cindent
set bdir=/home/baiguai/vim
set dir=/home/baiguai/vim
set backspace=2
set clipboard=unnamedplus
set expandtab

"-- SEARCH HIGHLIGHTING --
"noh
set hlsearch

"-- COMMANDS --
:command Dir :cd %:p:h

:command Cpyp :let @+ = expand("%:p")
:command Cpyf :let @+ = expand("%:t")

"-- LAUNCH VIM COMMANDER --
:noremap <silent> <F11> :cal VimCommanderToggle()<CR>

"-- YANKING --
set clipboard-=autoselect
``
