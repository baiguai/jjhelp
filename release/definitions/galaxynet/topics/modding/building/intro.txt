Introduction to Building
alias:building
In GalaxyNet players can use their collected minerals to build things.
What they can build is typically defined within:

``
[ROOT]
|__ library
    |__ build
``

But these definition directories/files can be stored anywhere within the *library*.
The [Adjustments](@adjustments,building) defined within the build menu point to the selected item to build's
directory *(Relative to the Galaxynet.jar file's directory)*.

Items that can be built are defined just like typical game elements are.
