Welcome to GalaxyNet!
alias:welcome
status:home
GalaxyNet is an arcade-style space mining game. It is driven by configuration files so that it
can be modded to the 'game author's' liking.

[menus](@menus,welcome)
