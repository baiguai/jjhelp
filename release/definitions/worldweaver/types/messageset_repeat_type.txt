MessageSet Repeat Types
keywords:messageset types messagesets types message set types message sets types
<em>none</em>
<ul>
    Displays its Messages in order. Once it's reached the last Message it
    continues to display that Message when it's fired.
</ul>

<em>random</em>
<ul>
    Randomly chooses one of its Messages each time it is fired.
</ul>

<em>repeat</em>
<ul>
    Displays its Messages in order. Once it has fired the last Message, next
    time it is fired it starts at the first one again, then works down them.
</ul>
