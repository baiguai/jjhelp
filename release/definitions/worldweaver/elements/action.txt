Actions
keywords:action elements action objects actions
<pre>
{actionset
    {action, type=<...>, source=<...>, newvalue=<...>, repeat=<...> }
}
</pre>

<br />

<strong>About</strong>
<ul>
    Actions within an ActionSet define either changes to the state of the game
    or they can simply call Events.
</ul>

<strong>Required Properties</strong>
<ul>
    <em>type</em>
    Specifies the Action's type. Each Action type has its own set of required
    properties, so be sure to consult the list of Action types that are
    available:
    <strong>action types</strong>
</ul>

<strong>Other Properties</strong>
<ul>
    <em>source</em>
    Specifies the source that the action is acting on or referencing.

<br />

    <em>newvalue</em>
    This value has different meanings depending on the Action type. It may be
    a new value to set or a location to travel to or move an object to etc.

<br />

    <em>repeat</em>
    Some Action types can use the repeat property to specify how often the
    action can fire etc.

<br />

    <em>sort</em>
    When this is set, it allows for custom processing sort order.
</ul>

<br />

Allowed Child Elements
<ul>
    <li> messageset</li>
</ul>
