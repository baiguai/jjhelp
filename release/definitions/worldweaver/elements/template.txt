Templates
alias:templates
keywords:template elements templates
A template is simply a Player, Object, or NPC definition that is written with
intent of spawning it. Every game will have at least one template - the Player
template within the game file. If you are making a standard Interactive Fiction
(IF) game your Player template may only have a couple Attributes in it like:


``
{player
    {attribute, alias=points, value=0 }
    {attribute, alias=goal, value=Find out who killed the butler }

    ...
}
``


However if you are building something more like a text-based First Person
Shooter (FPS), your template might look more like:


``
{player
    // ATTRIBUTES
    {attribute, alias=life, type=random, value=10\,24 }
    {attribute, type=max_life, type=attribute, value=life }
    {attribute, alias=speed, type=random, value=8\,18 }
    {attribute, alias=dexterity, type=random, value=7\,14 }
    {attribute, alias=initialized, value=false }
    {attribute, alias=level, value=1 }
    {attribute, alias=points, value=0 }
    {attribute, alias=goal, value=Escape the dungeon. }
    {attribute, alias=wearing_armor, value=false }

    {object, type=weapon, label=sword, meta=*sword
        ...
    }
}
``


#### Special Template Values
> There are special values that can be used for templates, for information on
> these see: 
> [special template values](@tmpl_val, templates).
