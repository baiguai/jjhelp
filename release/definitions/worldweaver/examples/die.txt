Die Setup Example
keywords:die examples dying examples end game examples game end examples kill player examples
<pre>
{room, alias=ntwk_sec_net, label=Security Net
    {attribute, alias=countdown, value=5 }

    // EVENTS
    {event, type=init|look
        {logicset
            {logic, type=attribute, source={self}:countdown, sourcevalue=1, operand== }
        }

        {actionset
            {action, type=attribute, source={self}:countdown, newvalue=0 }
        }

        {messageset, repeat=none
            [message
                A deafening siren rings through your mind. It is maddening. It icreases in
                intensity until your thoughts begin to scramble. Then black, and silence.
                The silence of death.
                \\n
                You have died.
            ]
        }
    }

    {event, type=init|look
        {logicset
            {logic, type=attribute, source={self}:countdown, sourcevalue=1, operand=&gt; }
        }

        {actionset
            {action, type=attribute, source={self}:countdown, newvalue={--} }
        }

        {messageset, repeat=none
            [message
                The Security Net materializes around you.
                It is an endless plane of configuration and status nodes. Each one is a large
                hovering triagle, glowing faintly in colors that reflect their statuses. They
                are too numerous to count. The nodes you see nearest you are labelled:
                \\n
                saturn base
                space port
                comm towers
                file server
                \\n
                Far overhead you see glowing text:
                ID Scan in: [att]{self}:countdown[/att].
            ]
            [message
                The nodes you see nearest you are labelled:
                \\n
                saturn base
                space port
                comm towers
                file server
                \\n
                Far overhead you see glowing text:
                ID Scan in: [att]{self}:countdown[/att].
            ]
        }
    }

    // This is defined last - because the game reset sets the countdown back to 4
    // TODO: When die events fire stop everything after it
    {event, type=init|look
        {logicset
            {logic, type=attribute, source={self}:countdown, sourcevalue=0, operand== }
        }

        {actionset
            {action, type=die }
        }
    }


    // COMMANDS
    {command, syntax=l|look
        {actionset
            {action, type=event, newvalue=look }
        }
    }
}
</pre>
