Game File / Directory Structure Example
keywords:examples game structure files folders directories
Each game you build must be in its own directory within the Games directory.
In this example the game is called The Dungeon. Remember you can use whatever
structure you like within the game's directory. This example shows a game
written in Norman Notation. If you are using XML the files names would use the
.xml file extension.


``
<root directory>
|__ Games
    |__ TheDungeon
        |__ Locations
        |   |__ Entrance
        |       |__ Room.nrmn
        |       |__ Objects.nrmn
        |__ game.nrmn
``


Notice the game.nrmn file. A file named 'game' is required in every game. It
specifies the properties etc of the game itself, and must be placed within the
game's root directory.
