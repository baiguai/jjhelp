Game Attributes
keywords:game attributes attribute values
Game level attributes must be specified using {game} as the element identifier.
Some special Game level Attributes and their values are:

<br />

<ul>
<em>display_mode</em>
The display mode Attribute specifies how game messages are displayed. If
this is not present or set to a value of clear messages are appended to the
bottom of the previous messages. If it is set to clear, the console is
cleared then the new messages are output.
</ul>
