Comments in Norman Notation
alias:nrmn_comments
keywords:norman notation comments
In its current iteration Norman notation only supports simple single line
comments.

They must be on their own line. Below is an example of multiple
lines of comments:

``
{room name=dungeon_entrance label=Dungeon Entrance
    // This is the starting point of the game.
    // The entrance is initially open, but will shut and
    // lock when the player enters.

    ...
}
``

As you can see comments are denoted by two forward slashes **//**.
