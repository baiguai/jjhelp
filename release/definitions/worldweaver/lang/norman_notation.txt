Norman Notation
alias:norman_notation
keywords:norman notation
#### Files
> Norman Notation files are standard text files that use the extension .nrmn
> For example: game.nrmn


#### Elements
> Norman Notation is very simple. There are only two types of elements that
> can be defined. Standard Elements and Text Elements.
> Standard elements can contain any number of properties and other elements.
> Text elements only contain text, this can be multiple lines of text.
> For more information on Elements see:
> **norman elements**


#### Properties
>  Standard elements can contain any number of properties, these can be defined
>  in the first line of the element code, or on their own lines throughout
>  the element.
>  For more information on Properties see:
>  <strong>norman properties</strong>


#### Comments
>  Comments are notes that authors can leave for themselves or others who
>  might work on the code in the future. They have no effect on the defined
>  elements or properties.
>  For more information on Comments see:
>  <strong>norman comments</strong>


#### Escaping
>  Sometimes text within properties or text elements run the risk of being
>  interpreted as the beginning or end of elements or properties. To avoid this
>  the characters can be 'escaped'.
>  For more information on this see:
>  <strong>norman escape</strong>


#### Examples
>  To really see Norman Notation in action, look at the example games defined
>  in:

``
[root]
|__ Games
``



#### See also:
> [norman examples](@examples_nrmn,norman_notation)
