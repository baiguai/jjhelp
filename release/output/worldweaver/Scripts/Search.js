var schix = 0;
var sPath = root + "Topics";

var searchDB = new Array();
var searchRef = new Array();

var checkEnter = function(e)
{ 
    // e is event object passed from function invocation
    // literal character code will be stored in this variable
    var characterCode;

    if(e && e.which){ 
        // if which property of event object is supported (NN4)
        e = e;
        // character code is contained in NN4's which property
        characterCode = e.which; 
    }
    else{
        try{
            e = event;
        }
        catch(err)
        {
            
        }
        
        // character code is contained in IE's keyCode property
        characterCode = e.keyCode; 
    }
    
        switch(characterCode)
    {
        case 13: // enter key
            exeSearch();
            return false;
            break;

        default:
            return true;
            break;
    }
}

var srchKey = function(e, ctrl)
{
    // e is event object passed from function invocation
    // literal character code will be stored in this variable
    var characterCode;

    if(e && e.which){ 
        // if which property of event object is supported (NN4)
        e = e;
        // character code is contained in NN4's which property
        characterCode = e.which; 
    }
    else{
        try{
            e = event;
        }
        catch(err)
        {
            
        }
        
        // character code is contained in IE's keyCode property
        characterCode = e.keyCode; 
    }
    
        switch(characterCode)
    {
        case 13: // enter key
            $(ctrl).triggerHandler("click");
            return false;
            break;

        default:
            return true;
            break;
    }
}

function exeSearch() {
    var oTxt = $('#txtSearch');
    var bExact = false;

    if ( $(oTxt).val().length == 0)
    {
        clearSearch();
        return true;
    }

    if ( $(oTxt).val().length < 3 ) {
        alert('Be sure to specify a search string that is at least four characters long.');
        return false;
    }

    var sSource = "";
    var win = $('#SearchResults'); //window.open("", "searchResults", "width=200,height=350");
    
    //window.name = "Main";
                    
    //win.document.open("text/html", "replace");
    $(win).html(searchDocs($(oTxt).val(), bExact));
    
    //win.document.close();
}
function clearSearch() {
    var oTxt = $('#txtSearch');
    var bExact = false;

    var sSource = "";
    var win = $('#SearchResults'); //window.open("", "searchResults", "width=200,height=350");

    $(win).html('');
    $('#txtSearch').val('');
    $('#txtSearch').focus();
}

function searchDocs(sCriteria,bExact) {
    var sTmp = "";
    var sSearch = "";

    sSearch = sCriteria;
    sSearch = sSearch.toLowerCase();

    var arTmp = sSearch.split(" ");

    if ( bExact ) {
        for (var oItem in searchDB) {
            var sEntry = "" + searchDB[oItem]["Topic"] + " " + searchDB[oItem]["Title"] + " " + searchDB[oItem]["Keywords"];
            sEntry = sEntry.toLowerCase();
            
            if ( sEntry.indexOf(sSearch) > -1 && isUniqueResult(searchDB[oItem]["Url"]) && searchDB[oItem]["Status"] != "deleted" && searchDB[oItem]["Status"] != "hidden" ) {
                if (searchDB[oItem]["Url"] == "")
                    sTmp += "                   <div onkeyup=\"srchKey(event, this);\" onclick=\"showTopic('" + searchDB[oItem]["ID"] + "'); searchHighlight('" + sCriteria + "');\" tabindex=\"0\">" + searchDB[oItem]["Title"] + "</div>\n";
                else
                    sTmp += "                   <a href=\"" + searchDB[oItem]["Url"] + "\" target=\"" + searchDB[oItem]["Target"] + "\">" + searchDB[oItem]["Title"] + "</a><br />\n";
            }
        }
    }
    else {
        for (var i in arTmp) {
            for (var oItem in searchDB) {
                var sEntry = "" + searchDB[oItem]["Topic"] + " " + searchDB[oItem]["Title"] + " " + searchDB[oItem]["Keywords"];
                sEntry = sEntry.toLowerCase();
                
                if ( sEntry.indexOf(arTmp[i]) > -1 && isUniqueResult(searchDB[oItem]["Url"]) && searchDB[oItem]["Status"] != "deleted" && searchDB[oItem]["Status"] != "hidden" ) {
                    if (searchDB[oItem]["Url"] == "")
                        sTmp += "                   <div onkeyup=\"srchKey(event, this);\" onclick=\"showTopic('" + searchDB[oItem]["ID"] + "'); searchHighlight('" + sCriteria + "');\" tabindex=\"0\">" + searchDB[oItem]["Title"] + "</div>\n";
                else
                    sTmp += "                   <div><a href=\"" + searchDB[oItem]["Url"] + "\" target=\"" + searchDB[oItem]["Target"] + "\">" + searchDB[oItem]["Title"] + "</a></div>\n";
                }
            }
        }
    }

    if (sTmp == "")
    {
        sTmp += "<div>There are no results to display</div>";
    }
    
    sTmp += "        </table>\n";

    return sTmp;
}

function linkTopic(oItem, oSource) {
    var div = $('.MainContent');
    var html = '';
    var back = '';
    var srcID = '';

    for (var i=0; i < searchDB.length; i++)
    {
        if (oSource != null && oSource != '')
        {
            if(searchDB[i]["Alias"] == oSource)
            {
                srcID = searchDB[i]["ID"];
                break;
            }
        }
    }

    for (var i=0; i < searchDB.length; i++)
    {
        if(searchDB[i]["Alias"] == oItem)
        {
            html += '<h1 class="TopicTitle">' + searchDB[i]["Title"] + '</h1><h2 class="TopicSubTitle">' + searchDB[i]["SubTitle"] + '</h2><hr />';
            if (srcID != '') back = '<a href="javascript:showTopic(\''+srcID+'\');">&lt; back</a><br /><br />';
            html += searchDB[i]["Topic"];

            var node = $tree.tree('getNodeById', searchDB[i]["ID"]);
            $tree.tree('selectNode', node);

            $(div).html(back + $(div).html() + "<br /><br />" + back);

            break;
        }
    }

    $('.LightBox').fancybox();
}
function showTopic(oItem, oSource) {
    var div = $('.MainContent');
    var html = '';

    for (var i=0; i < searchDB.length; i++)
    {
        if(searchDB[i]["ID"] == oItem)
        {
            html += '<h1 class="TopicTitle">' + searchDB[i]["Title"] + '</h1><h2 class="TopicSubTitle">' + searchDB[i]["SubTitle"] + '</h2><hr />';
            if (oSource != null && oSource != '') html += '<a href="javascript:showTopic(\''+oSource+'\');">&lt; back</a><br /><br />';
            html += searchDB[i]["Topic"];
            if (oSource != null && oSource != '') html += '<br /><br /><a href="javascript:showTopic(\''+oSource+'\');">&lt; back</a>';

            // insert the global vars
            for (var i2 = 0; i2 < Vars.length; i2++)
            {
                html = Replace(html, '@@'+Vars[i2]["key"]+'@@', Vars[i2]["value"]);
            }

            $(div).html(html);
            
            var node = $tree.tree('getNodeById', oItem);
            $tree.tree('selectNode', node);
            break;
        }
    }

    $('.LightBox').fancybox();
}
function UniqueTopicCheck(oItem) {
    var isUnique  = true;

    for (var i=0; i < searchDB.length; i++)
    {
        if(searchDB[i]["ID"] == oItem)
        {
            isUnique = false;
            break;
        }
    }
    
    return isUnique;
}

function showHome() {
    for (var oItem in searchDB) {
        if ( searchDB[oItem]["Status"] == "home" && $('.MainContent') != null) {
            if (searchDB[oItem]["SubTitle"] != "")
                $('.MainContent').html('<h1 class="TopicTitle">' + searchDB[oItem]["Title"] + '</h1><h5>' + searchDB[oItem]["SubTitle"] + '</h5><hr />' + searchDB[oItem]["Topic"]);
            else
                $('.MainContent').html('<h1 class="TopicTitle">' + searchDB[oItem]["Title"] + '</h1><hr />' + searchDB[oItem]["Topic"]);

            var node = $tree.tree('getNodeById', searchDB[oItem]["ID"]);
            $tree.tree('selectNode', node);
        }
    }
}

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");

    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
    
        if (pair[0] == variable) {
            return pair[1];
        }
    } 

    return "";
}

function isUniqueResult(oArray,sUrl) {
    var sTmp = "";
    var sSearch = "";
    sSearch = sUrl;
    sSearch = sTmp.toLowerCase();
    bSuccess = true;
    
    for (var oItem in oArray) {
        // check against the url
        sTmp = oArray[oItem]["Link"];
        
        if ( sTmp == sSearch ) {
            bSuccess = false;
        }
    }
    
    return bSuccess;
}








/*
                        SEARCH WORD HIGHLIGHTER CODE
*/
/* http://www.kryogenix.org/code/browser/searchhi/ */
/* Modified 20021006 to fix query string parsing and add case insensitivity */
function highlightWord(node,word) {
    // Iterate into this nodes childNodes
    if (node.hasChildNodes) {
        var hi_cn;
        for (hi_cn=0;hi_cn<node.childNodes.length;hi_cn++) {
            highlightWord(node.childNodes[hi_cn],word);
        }
    }

    // And do this node itself
    if (node.nodeType == 3) { // text node
        tempNodeVal = node.nodeValue.toLowerCase();
        tempWordVal = word.toLowerCase();
        if (tempNodeVal.indexOf(tempWordVal) != -1) {
            pn = node.parentNode;
            if (pn.className != "searchword") {
                // word has not already been highlighted!
                nv = node.nodeValue;
                ni = tempNodeVal.indexOf(tempWordVal);
                // Create a load of replacement nodes
                before = document.createTextNode(nv.substr(0,ni));
                docWordVal = nv.substr(ni,word.length);
                after = document.createTextNode(nv.substr(ni+word.length));
                hiwordtext = document.createTextNode(docWordVal);
                hiword = document.createElement("span");
                hiword.className = "searchword";
                hiword.appendChild(hiwordtext);
                pn.insertBefore(before,node);
                pn.insertBefore(hiword,node);
                pn.insertBefore(after,node);
                pn.removeChild(node);
            }
        }
    }
}

function googleSearchHighlight() {
    if (getQueryVariable("srch") != "") {
        tmp = getQueryVariable("srch");

        words = tmp.split('%20');

        for (w=0;w<words.length;w++) {
            highlightWord(document.getElementsByTagName("body")[0],words[w]);
        }
    }
}
function searchHighlight(tmp) {
    words = tmp.split('%20');

    for (w=0;w<words.length;w++) {
        $('.MainContent').each(function()
        {
            highlightWord($(this)[0],words[w]);
        });
        //highlightWord(document.getElementsByTagName("body")[0],words[w]);
    }
}

function loadDefaults() {
    var tid = getQueryVariable("tid");
    
    if (tid == "") 
    {
        showHome();
    }
    else
    {
        googleSearchHighlight();
        showTopic(tid);
    }
}

var Replace = function(stringIn, oldText, newText)
{
    stringIn = stringIn.replace(new RegExp(oldText, "gi"), newText);
    return stringIn;
}
