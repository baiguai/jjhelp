var TreeData = [{"name":"Horonet","id":"825eaa35-eb8f-469d-9ae0-92c9fd603e92","is_open":false,"children":[{"name":"Introduction","id":"040694cd-dfb0-4a28-be09-06ae3e0967a4","is_open":false},{"name":"Watches","id":"6f1949b4-0d97-4bef-8b7d-611aaab1b42f","is_open":false,"children":[{"name":"By Brand","id":"e04e4387-0d87-4a13-ba8b-cc4611ca8532","is_open":false,"children":[{"name":"Cartier","id":"54764695-139f-45a3-896b-b6fda78730e3","is_open":false,"children":[{"name":"Tank Americaine","id":"ed54a4c4-50b4-44b2-babd-be4688961291","is_open":false}]},{"name":"Casio","id":"a69aed52-87ce-47a0-bbb7-f21bccc24c63","is_open":false,"children":[{"name":"F-91W","id":"a61452c3-044c-4e93-b833-1e3d2f6336a3","is_open":false}]}]},{"name":"By Style","id":"958c131e-91d7-454d-93ab-3efd3cf3dc9e","is_open":false,"children":[{"name":"Digital / SciFi","id":"aab249e9-d7cb-4dfa-8be6-249d1848af01","is_open":false,"children":[{"name":"F-91W","id":"349ffa2f-848b-454a-8fa3-95aa68e28002","is_open":false}]},{"name":"Dress","id":"cf29e184-8025-45d8-b8af-99b1d57b09b7","is_open":false,"children":[{"name":"Tank Americaine","id":"b29fc77a-e27b-439c-ad3c-a93d7f155ec0","is_open":false}]}]}]}]}]

searchDB[schix] = new Array();
searchDB[schix]["ID"] = "040694cd-dfb0-4a28-be09-06ae3e0967a4";
searchDB[schix]["Title"] = "Introduction";
searchDB[schix]["SubTitle"] = "";
searchDB[schix]["Url"] = "";
searchDB[schix]["Target"] = "";
searchDB[schix]["Description"] = "";
searchDB[schix]["Topic"] = "Horonet is an ever-growing database of watches, movements, watch accessories.<br />Each item has been hand-coded and painstakingly researched. The items in this database<br />cover both commonly sought after pieces and items as well as <em>out of the way</em> items.<br /><br /><img alt=\"Tank\" src=\"Images/watches/tank.png\" style=\"width: 200px\"  /><br />";
searchDB[schix]["Keywords"] = "";
searchDB[schix]["Categories"] = "";
searchDB[schix]["Status"] = "";
searchDB[schix]["Alias"] = "";
searchRef[searchDB[schix]["ID"]] = searchDB[schix]["Url"]; schix ++;

searchDB[schix] = new Array();
searchDB[schix]["ID"] = "ed54a4c4-50b4-44b2-babd-be4688961291";
searchDB[schix]["Title"] = "Tank Americaine Medium";
searchDB[schix]["SubTitle"] = "";
searchDB[schix]["Url"] = "";
searchDB[schix]["Target"] = "";
searchDB[schix]["Description"] = "";
searchDB[schix]["Topic"] = "<img alt=\"Tank Americaine\" src=\"Images/watches/cartier_tank-americaine_WSTA0017__01.png\" style=\"width: 300px\"  /><br /><br /><table cellspacing='0' class='content_table'><tr><th>Attribute</th><th>Value</th></tr><tr><td style='width: 150px;'>Movement Type</td><td>Automatic</td></tr><tr><td style='width: 150px;'>Movement</td><td>??</td></tr><tr><td style='width: 150px;'>Shape</td><td>Rectangle</td></tr><tr><td style='width: 150px;'>Case Width</td><td>22.6mm</td></tr><tr><td style='width: 150px;'>Case Height</td><td>41.6mm</td></tr><tr><td style='width: 150px;'>Case Thickness</td><td>9.5mm</td></tr><tr><td style='width: 150px;'>Lug Width</td><td>19mm</td></tr><tr><td style='width: 150px;'>Crystal</td><td>sapphire</td></tr><tr><td style='width: 150px;'>Style</td><td>Dress</td></tr>";
searchDB[schix]["Keywords"] = "";
searchDB[schix]["Categories"] = "";
searchDB[schix]["Status"] = "";
searchDB[schix]["Alias"] = "";
searchRef[searchDB[schix]["ID"]] = searchDB[schix]["Url"]; schix ++;

searchDB[schix] = new Array();
searchDB[schix]["ID"] = "a61452c3-044c-4e93-b833-1e3d2f6336a3";
searchDB[schix]["Title"] = "F-91W";
searchDB[schix]["SubTitle"] = "";
searchDB[schix]["Url"] = "";
searchDB[schix]["Target"] = "";
searchDB[schix]["Description"] = "";
searchDB[schix]["Topic"] = "</table> <img alt=\"Casio F-91W\" src=\"Images/watches/Casio_F-91W__01.png\" style=\"width: 300px\"  /><br /><br /><table cellspacing='0' class='content_table'><tr><th>Attribute</th><th>Value</th></tr><tr><td style='width: 150px;'>Movement Type</td><td>Quartz</td></tr><tr><td style='width: 150px;'>Movement</td><td>??</td></tr><tr><td style='width: 150px;'>Shape</td><td>Square</td></tr><tr><td style='width: 150px;'>Case Width</td><td>33.5mm</td></tr><tr><td style='width: 150px;'>Case Height</td><td>37.5mm</td></tr><tr><td style='width: 150px;'>Case Thickness</td><td>8.5mm</td></tr><tr><td style='width: 150px;'>Lug Width</td><td>18mm</td></tr><tr><td style='width: 150px;'>Crystal</td><td>glass</td></tr><tr><td style='width: 150px;'>Style</td><td>Digital</td></tr>";
searchDB[schix]["Keywords"] = "";
searchDB[schix]["Categories"] = "";
searchDB[schix]["Status"] = "";
searchDB[schix]["Alias"] = "";
searchRef[searchDB[schix]["ID"]] = searchDB[schix]["Url"]; schix ++;

searchDB[schix] = new Array();
searchDB[schix]["ID"] = "349ffa2f-848b-454a-8fa3-95aa68e28002";
searchDB[schix]["Title"] = "F-91W";
searchDB[schix]["SubTitle"] = "";
searchDB[schix]["Url"] = "";
searchDB[schix]["Target"] = "";
searchDB[schix]["Description"] = "";
searchDB[schix]["Topic"] = "</table> <img alt=\"Casio F-91W\" src=\"Images/watches/Casio_F-91W__01.png\" style=\"width: 300px\"  /><br /><br /><table cellspacing='0' class='content_table'><tr><th>Attribute</th><th>Value</th></tr><tr><td style='width: 150px;'>Movement Type</td><td>Quartz</td></tr><tr><td style='width: 150px;'>Movement</td><td>??</td></tr><tr><td style='width: 150px;'>Shape</td><td>Square</td></tr><tr><td style='width: 150px;'>Case Width</td><td>33.5mm</td></tr><tr><td style='width: 150px;'>Case Height</td><td>37.5mm</td></tr><tr><td style='width: 150px;'>Case Thickness</td><td>8.5mm</td></tr><tr><td style='width: 150px;'>Lug Width</td><td>18mm</td></tr><tr><td style='width: 150px;'>Crystal</td><td>glass</td></tr><tr><td style='width: 150px;'>Style</td><td>Digital</td></tr>";
searchDB[schix]["Keywords"] = "";
searchDB[schix]["Categories"] = "";
searchDB[schix]["Status"] = "";
searchDB[schix]["Alias"] = "";
searchRef[searchDB[schix]["ID"]] = searchDB[schix]["Url"]; schix ++;

searchDB[schix] = new Array();
searchDB[schix]["ID"] = "b29fc77a-e27b-439c-ad3c-a93d7f155ec0";
searchDB[schix]["Title"] = "Tank Americaine Medium";
searchDB[schix]["SubTitle"] = "";
searchDB[schix]["Url"] = "";
searchDB[schix]["Target"] = "";
searchDB[schix]["Description"] = "";
searchDB[schix]["Topic"] = "</table> <img alt=\"Tank Americaine\" src=\"Images/watches/cartier_tank-americaine_WSTA0017__01.png\" style=\"width: 300px\"  /><br /><br /><table cellspacing='0' class='content_table'><tr><th>Attribute</th><th>Value</th></tr><tr><td style='width: 150px;'>Movement Type</td><td>Automatic</td></tr><tr><td style='width: 150px;'>Movement</td><td>??</td></tr><tr><td style='width: 150px;'>Shape</td><td>Rectangle</td></tr><tr><td style='width: 150px;'>Case Width</td><td>22.6mm</td></tr><tr><td style='width: 150px;'>Case Height</td><td>41.6mm</td></tr><tr><td style='width: 150px;'>Case Thickness</td><td>9.5mm</td></tr><tr><td style='width: 150px;'>Lug Width</td><td>19mm</td></tr><tr><td style='width: 150px;'>Crystal</td><td>sapphire</td></tr><tr><td style='width: 150px;'>Style</td><td>Dress</td></tr>";
searchDB[schix]["Keywords"] = "";
searchDB[schix]["Categories"] = "";
searchDB[schix]["Status"] = "";
searchDB[schix]["Alias"] = "";
searchRef[searchDB[schix]["ID"]] = searchDB[schix]["Url"]; schix ++;

