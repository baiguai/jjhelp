var $tree = $('#TreeviewWrapper');

$(function() {
    $('#TreeviewWrapper').tree({
        data: TreeData,
        dragAndDrop: false
    });

    $('#TreeviewWrapper').bind(
        'tree.select',
        function(event) {
            if (event.node != null)
            {
                SelectNode(event.node.id);
            }
        }
    );
    
    $tree = $('#TreeviewWrapper');
    CollapseAllNodes();
});
function BuildTreeData()
{
    return $tree.tree('toJson');
}

function CollapseAllNodes()
{
    var tree = $tree.tree('getTree');

    tree.iterate(function(node) {
        if (node.hasChildren()) {
            $tree.tree('closeNode', node, false);
        }
        return true;
    });
}
function ExpandAllNodes()
{
    var tree = $tree.tree('getTree');

    tree.iterate(function(node) {
        if (node.hasChildren()) {
            $tree.tree('openNode', node, false);
        }
        return true;
    });
}

function AddNode()
{
    currentParent = $tree.tree('getSelectedNode');
    var newID = 'Node_' + GenerateID();

    if (currentParent == null)
    {
        $tree.tree(
            'appendNode',
            {
                label: 'New Item',
                id: newID
            }
        );
    }
    else
    {
        if (!UniqueTopicCheck(currentParent.id))
        {
            alert('You cannot create an entry within the current item');
            event.preventDefault();
            return;
        }
        
        $tree.tree(
            'appendNode',
            {
                label: 'New Item',
                id: newID
            },
            currentParent
        );
        
        $tree.tree('openNode', currentParent, true);
    }

    var node = $tree.tree('getNodeById', newID);
    $tree.tree('selectNode', node);
    RenameNode();

    buildLeftNavCode();
}
function DeleteNode()
{
    var tree = $tree.tree('getTree');
    var node = $tree.tree('getSelectedNode');
    var hasNodes = false;
    $tree.tree('removeNode', node);
    
    tree.iterate(function(node) {
        hasNodes = true;
    });
    
    if (!hasNodes) AddNode();
    
    buildLeftNavCode();
}
function MoveNode(event)
{
    if (!UniqueTopicCheck(event.move_info.target_node.id))
    {
        alert('Be sure to drag the ' + event.move_info.moved_node.name + ' element to a blank element');
        event.preventDefault();
    }
    
    buildLeftNavCode();
}
function RenameNode()
{
    var node = $tree.tree('getSelectedNode');
    var newName = prompt("Please enter your name", node.name);
    if (newName != null) {
        $tree.tree('updateNode', $tree.tree('getSelectedNode'), newName);
    }
    
    $tree.tree('selectNode', node);
    $('#txtTitle').val(newName);
    buildLeftNavCode();
}
function SelectNode(nodeID)
{
    var tree = $tree.tree('getTree');
    var parentNode = $tree.tree('getNodeById', nodeID);
    var parentName = '';

    currentParent = parentNode;
    currentItem = null;
    
    if (parentNode.hasChildren()) {
        $tree.tree('toggle', parentNode);
    }

    if (parentNode != null && !HasChildren(parentNode))
    {
        if (!UniqueTopicCheck(parentNode.id))
        {
            currentItem = parentNode;
            currentID = parentNode.id;
            showTopic(parentNode.id, null);
            parentNode = parentNode.parent;
            parentName = parentNode.name;
            currentParent = parentNode;
        }
        else
        {
            if (parentNode.parent.id == null)
            {
                currentItem = null;
                currentID = null;
                parentNode = parentNode;
                parentName = parentNode.name;
                currentParent = parentNode;
            }
            else
            {
                parentName = '<a href="javascript:SelectNode(' + parentNode.parent.id + ');">' + parentNode.parent.name + '</a> &nbsp; &gt; &nbsp; ' + parentNode.name;
            }
            
            showTopic(0, null);
        }
    }
    else
    {
        parentName = parentNode.name;
    }

    $('#spnCurrentFolder').html(parentName);
}
function HasChildren(parentNode)
{
    var hasChildren = false;
    
    try
    {
        if (parentNode.hasChildren())
        {
            hasChildren = true;
        }
    }
    catch(exception) { }

    return hasChildren;
}
/* END THE TREEVIEW CODE */





















