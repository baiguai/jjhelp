currentParent = null;
currentItem = null;
currentID = null;

function Refresh()
{
    $('#divAdminTree').html(getTree());
}

function generateNewCode()
{
    currentItem = null;
    currentID = GenerateID();

    if (currentParent != null && !UniqueTopicCheck(currentParent.id))
    {
        alert('You cannot add an item to the selected tree node');
        return;
    }
    
    if (currentParent == null)
    {
        $tree.tree(
            'appendNode',
            {
                label: $('#txtTitle').val(),
                id: currentID
            }
        );
    }
    else
    {
        $tree.tree(
            'appendNode',
            {
                label: $('#txtTitle').val(),
                id: currentID
            },
            currentParent
        );
        
        $tree.tree('openNode', currentParent, true);
    }

    generateCode();
}

function generateCode(globalRebuild) {
    var pound = /#/g;

    if (!globalRebuild)
    {
        // validate the title
        if ( currentID == null ) {
             currentID = GenerateID();
        }
        
        var sCaption = $('#txtCaption').val();
        // validate the caption
        if ( sCaption.length < 1 ) {
            alert('Enter a caption for the left nav tree');
            return false;
        }
        
        if (currentItem != null)
        {
            currentItem.name = sCaption;
        }
        
        var sTitle = $('#txtTitle').val();
        // validate the title
        if ( sTitle.length < 1 ) {
            alert('Enter a title');
            return false;
        }
        
        var sSubTitle = $('#txtSubTitle').val();
        
        var sTopic = tinymce.get('txtTopic').getContent();
        
        var sKeywords = $('#txtKeywords').val();
        
        var sCats = $('#txtCategories').val();
        
        var sLink = $('#txtLink').val();
        
        var sTarget = document.getElementById('txtTarget').value;
        
        var chkIsHome = false;
        if (document.getElementById('chkHome').checked) chkIsHome = true;
    }
    
    // build the items
    buildCode(currentID,sTitle,sSubTitle,sKeywords,sCats,sTopic,sLink,sTarget,chkIsHome);
    //buildLeftNavCode(currentID);
    
    return true;
}

/*
function buildLeftNavCode(eID) {
    sTmp = "";
    var sCaption = document.getElementById("txtCaption").value;
    var sLink = document.getElementById("txtLink").value;
    var sTarget = document.getElementById("txtTarget").value;
    //var txt = document.getElementById("txtLeftNavCode");
    var txt = document.getElementById("txtResEntryArray");
    var oParent = document.getElementById("cboParent");

    txt.value = 'var TreeData = ' + BuildTreeData();
    return;
}
*/

function buildCode(sID, sTitle, sSubTitle, sKeywords, sCategories, sTopic, sLink, sTarget, isHome) {
    var sDescription = "";
    var sCode = "";
    var sStatus = "";
    var txt = document.getElementById("txtResEntryArray");
    
    txt.value = 'var TreeData = ' + BuildTreeData();

    // build the existing entries
    for (var oItem in searchDB) {
        if (searchDB[oItem]['ID'] != sID && searchDB[oItem]['Topic'] != '')
        {
            sCode += "searchDB[schix] = new Array();\n";
            sCode += "searchDB[schix][\"ID\"] =                 \"" + scrubText(searchDB[oItem]['ID']) + "\";\n";
            sCode += "searchDB[schix][\"Title\"] =              \"" + scrubText(searchDB[oItem]['Title']) + "\";\n";
            sCode += "searchDB[schix][\"SubTitle\"] =           \"" + scrubText(searchDB[oItem]['SubTitle']) + "\";\n";
            sCode += "searchDB[schix][\"Url\"] =                \"" + scrubText(searchDB[oItem]['Url']) + "\";\n";
            sCode += "searchDB[schix][\"Target\"] =             \"" + scrubText(searchDB[oItem]['Target']) + "\";\n";
            sCode += "searchDB[schix][\"Description\"] =        \"" + scrubText(searchDB[oItem]['Description']) + "\";\n";
            sCode += "searchDB[schix][\"Topic\"] =              \"" + scrubText(searchDB[oItem]['Topic']) + "\";\n";
            sCode += "searchDB[schix][\"Keywords\"] =           \"" + scrubText(searchDB[oItem]['Keywords']) + "\";\n";
            if (searchDB[oItem]['Categories'] == null)
            {
                sCode += "searchDB[schix][\"Categories\"] =         \"\";\n";
            }
            else
            {
                sCode += "searchDB[schix][\"Categories\"] =         \"" + scrubText(searchDB[oItem]['Categories']) + "\";\n";
            }
            sCode += "searchDB[schix][\"Status\"] =             \"" + scrubText(searchDB[oItem]['Status']) + "\";\n";
            sCode += "    searchRef[searchDB[schix][\"ID\"]] = searchDB[schix][\"Url\"]; schix ++;\n\n";
        }
    }

    // clean up the strings
    sTitle = scrubText(sTitle);
    sKeywords = scrubText(sKeywords);
    sCategories = scrubText(sCategories);
    sSubTitle = scrubText(sSubTitle);
    sTopic = scrubText(sTopic);
    sLink = scrubText(sLink);
    sTarget = scrubText(sTarget);

    sCode += "searchDB[schix] = new Array();\n";
    sCode += "searchDB[schix][\"ID\"] =                 \"" + sID + "\";\n";
    sCode += "searchDB[schix][\"Title\"] =              \"" + sTitle + "\";\n";
    sCode += "searchDB[schix][\"SubTitle\"] =           \"" + sSubTitle + "\";\n";
    sCode += "searchDB[schix][\"Url\"] =                \"" + sLink + "\";\n";
    sCode += "searchDB[schix][\"Target\"] =                \"" + sTarget + "\";\n";
    sCode += "searchDB[schix][\"Description\"] =        \"" + sDescription + "\";\n";
    sCode += "searchDB[schix][\"Topic\"] =                \"" + sTopic + "\";\n";
    sCode += "searchDB[schix][\"Keywords\"] =           \"" + sKeywords + "\";\n";
    sCode += "searchDB[schix][\"Categories\"] =            \"" + sCategories + "\";\n";
    if (isHome) sCode += "searchDB[schix][\"Status\"] =                 \"home\";\n";
    else sCode += "searchDB[schix][\"Status\"] =                    \"\";\n";
    sCode += "    searchRef[searchDB[schix][\"ID\"]] = searchDB[schix][\"Url\"]; schix ++;\n";
    
    txt.value += '\n\n' + sCode;
    
    return true;
}

function GenerateID()
{
    newID = '';
    guid = Guid();
    
    newID = guid;
    
    if (uniqueIdCheck(newID) == false) {
        newID = GenerateID();
    }

    return newID;
}
function Guid()
{
    var sGuid="";
    for (var i=0; i<32; i++)
    {
        sGuid+=Math.floor(Math.random()*0xF).toString(0xF);
    }
    return sGuid;
}


function copyCode()
{
    var copyTextarea = $('#txtResEntryArray');
    copyTextarea[0].select();
    
    try {
        var successful = document.execCommand('copy');
        
        if (successful)
        {
            alert('The code has been successfully copied');
        }
        else
        {
            alert('An error occurred.');
        }
        
        //console.log('Copying text command was ' + msg);
    } 
    catch (err) {
        console.log('Oops, unable to copy');
    }
}

function launchData()
{
    window.open('Data', 'Data/Data.js');
}

//function loadTopic(eID) {
function showTopic(eID) {
    var ix = getSearchDBIndex(eID);
    var oID = document.getElementById("txtID")
    var oTitle = document.getElementById("txtTitle");
    var oCaption = document.getElementById("txtCaption");
    var oSubTitle = document.getElementById("txtSubTitle");
    var oKeywords = document.getElementById("txtKeywords");
    var oCategories = $('#txtCategories');
    var oLink = document.getElementById("txtLink");
    var oTarget = document.getElementById("txtTarget");
    var oTopic = document.getElementById("txtTopic");
    var oEncode = document.getElementById("txtEncodeText");
    //var oTreeCode = document.getElementById("txtLeftNavCode");
    var oParent = document.getElementById("cboParent");
    var oIsHome = document.getElementById("chkHome");
    var src = "";
    //var oTree = getTree();
    var parentID = "";
    var fTitle = "";

    $('.Message').html('');

    // get the parent id
    /*
    for (var n=0; n<oTree.aNodes.length; n++)
    {
        if (oTree.aNodes[n].id == eID) 
        {
            parentID = oTree.aNodes[n].pid;
            fTitle = oTree.aNodes[n].title;
            break;
        }
    }
    */

    if (searchDB[ix] == null)
    {
        document.getElementById("txtSubTitle").value = "";
        document.getElementById("txtKeywords").value = "";
        document.getElementById("txtCategories").value = "";
        document.getElementById("txtLink").value = "";
        document.getElementById("txtTarget").value = "";
        //document.getElementById("txtTopic").value = "";
        tinymce.get('txtTopic').setContent('');
        document.getElementById("txtTitle").value = "";
        document.getElementById("txtCaption").value = fTitle;
        //document.getElementById("txtLeftNavCode").value = "";
        //showFolder(parentID, eID, fTitle);
        return;
    }
    
    oID.value = searchDB[ix]["ID"];
    oTitle.value = searchDB[ix]["Title"];
    oCaption.value = currentItem.name;
    oSubTitle.value = searchDB[ix]["SubTitle"];
    oKeywords.value = searchDB[ix]["Keywords"];
    oCategories.value = searchDB[ix]["Categories"];
    oLink.value = searchDB[ix]["Url"];
    oTarget.value = searchDB[ix]["Target"];
    //oTopic.value = searchDB[ix]["Topic"];
    tinymce.get('txtTopic').setContent(searchDB[ix]["Topic"]);
    oEncode.value = '';
    
    if (searchDB[ix]["Status"] == "home") oIsHome.checked = true;
    else oIsHome.checked = false;
}

uniqueIdCheck = function(sID)
{
    var isUnique = true;
    
    if (isUnique == true)
    {
        for (var oItem in searchDB)
        {
            if (searchDB[oItem]["ID"] == sID) isUnique = false;
            break;
        }
    }
    
    return isUnique;
}

getSearchDBIndex = function(sID)
{
    var ix = 0;
    
    for (var oItem in searchDB)
    {
        if (searchDB[oItem]["ID"] == sID) break;
        ix++;
    }
    
    return ix;
}

function scrubText(valueIn) {
    var dubQuot = /\"/g;
    var lineReturn = /\r/g;
    var newLine = /\n/g;
    var backSlash = /\\/g;

    if (valueIn == null) return valueIn;

    valueIn = valueIn.replace(backSlash, "\\\\");
    valueIn = valueIn.replace(dubQuot, "\\\"");
    valueIn = valueIn.replace(lineReturn, "\\r");
    valueIn = valueIn.replace(newLine, "\\n");
    valueIn = valueIn.replace(lineReturn, " ");
    valueIn = valueIn.replace(newLine, " ");
    
    return valueIn;
}

function scrubLeftText(valueIn) {
    var singQuot = /\'/g;
    var lineReturn = /\r/g;
    var newLine = /\n/g;
    var backSlash = /\\/g;

    if (valueIn == null) return valueIn;

    valueIn = valueIn.replace(backSlash, "\\\\");
    valueIn = valueIn.replace(singQuot, "\\\'");
    valueIn = valueIn.replace(lineReturn, "\\r");
    valueIn = valueIn.replace(newLine, "\\n");
    valueIn = valueIn.replace(lineReturn, " ");
    valueIn = valueIn.replace(newLine, " ");
    
    return valueIn;
}

function rawText(valueIn) {
    var openBrack = /</g;
    var closeBrack = />/g;
    var dubQuot = /\"/g;
    var lineReturn = /\r/g;
    var newLine = /\n/g;
    var backSlash = /\\/g;

    valueIn = valueIn.replace(backSlash, "");
    valueIn = valueIn.replace(dubQuot, "");
    valueIn = valueIn.replace(openBrack, "");
    valueIn = valueIn.replace(closeBrack, "");
    valueIn = valueIn.replace(lineReturn, " ");
    valueIn = valueIn.replace(newLine, " ");
    
    return valueIn;
}

function setTitle(txt) {
    obj = document.getElementById("txtTitle");
    
    if (obj.value == "") obj.value = txt.value;
}

function clearAll() {
    ClearFields('.FieldsTable');
    $('.Message').html('');
}


// SEARCH METHODS
function exeSearch() {
    var oTxt = document.getElementById("txtSearch");
    var bExact = false;
    
    if ( oTxt.value.length < 3 ) {
        alert("Be sure to specify a search string that is at least four characters long.");
        return false;
    }
    
    var sSource = "";
    var win = document.getElementById("SearchResults"); //window.open("", "searchResults", "width=200,height=350");
    
    //window.name = "Main";
                    
    //win.document.open("text/html", "replace");
    win.innerHTML = searchDocs(oTxt.value,bExact);
    
    //win.document.close();
}

function isUniqueResult(oArray,sUrl) {
    var sTmp = "";
    var sSearch = "";
    sSearch = sUrl;
    sSearch = sTmp.toLowerCase();
    bSuccess = true;
    
    for (var oItem in oArray) {
        // check against the url
        sTmp = oArray[oItem]["Link"];
        
        if ( sTmp == sSearch ) {
            bSuccess = false;
        }
    }
    
    return bSuccess;
}

var fromID = "";
var toID = "";

function SetFrom()
{
    var txtID = document.getElementById("txtID");
    var spnFrom = document.getElementById("spnFrom");
    fromID = txtID.value;
    spnFrom.innerHTML = fromID;
}
function UnSetFrom()
{
    var spnFrom = document.getElementById("spnFrom");
    fromID = "";
    spnFrom.innerHTML = fromID;
}
function SetTo()
{
    var txtID = document.getElementById("txtID");
    var spnTo = document.getElementById("spnTo");
    toID = txtID.value;
    spnTo.innerHTML = toID;
}
function GenerateLink()
{
    var txtLinkText = document.getElementById("txtLinkText");
    var preLink = document.getElementById("preLink");
    if (fromID != "")
        preLink.innerHTML = "javascript:showTopic('" + toID + "', '" + fromID + "');";
    else
        preLink.innerHTML = "javascript:showTopic('" + toID + "');";
}

function EncodeText(idIn, encode)
{
    var obj = document.getElementById(idIn);
    var txtIn = obj.value;
    var pre = document.getElementById("preOutput");
    var chk = document.getElementById("chkType");
    var out = '';

    out = txtIn;

    if (encode)
    {
        out = out.replace(/&/g, "&amp;");
        out = out.replace(/</g, "&lt;");
        out = out.replace(/>/g, "&gt;");
    }
    else
    {
        out = out.replace(/&lt;/g, "<");
        out = out.replace(/&gt;/g, ">");
        out = out.replace(/&amp;/g, "&");
    }

    obj.value = out;
}






/* BEGIN THE TREEVIEW CODE */
var $tree = $('#TreeviewWrapper');

$(function() {
    $('#TreeviewWrapper').tree({
        data: TreeData,
        dragAndDrop: true
    });

    $('#TreeviewWrapper').bind(
        'tree.select',
        function(event) {
            if (event.node != null)
            {
                SelectNode(event.node.id);
            }
        }
    );
    
    $('#TreeviewWrapper').bind(
        'tree.move',
        function(event) {
            MoveNode(event);
        }
    );
    
    $tree = $('#TreeviewWrapper');
    
    $tree.jqTreeContextMenu($('#myMenu'), {
        "rename": function (node) {
            RenameNode();
        },
        "delete": function (node) {
            DeleteNode();
        },
        "add": function (node) { 
            AddNode();
        }
    });
});
function BuildTreeData()
{
    return $tree.tree('toJson');
}

function CollapseAllNodes()
{
    var tree = $tree.tree('getTree');

    tree.iterate(function(node) {
        if (node.hasChildren()) {
            $tree.tree('closeNode', node, false);
        }
        return true;
    });
}
function ExpandAllNodes()
{
    var tree = $tree.tree('getTree');

    tree.iterate(function(node) {
        if (node.hasChildren()) {
            $tree.tree('openNode', node, false);
        }
        return true;
    });
}

function AddNode()
{
    currentParent = $tree.tree('getSelectedNode');
    var newID = 'Node_' + GenerateID();

    if (currentParent == null)
    {
        $tree.tree(
            'appendNode',
            {
                label: 'New Item',
                id: newID
            }
        );
    }
    else
    {
        if (!UniqueTopicCheck(currentParent.id))
        {
            alert('You cannot create an entry within the current item');
            event.preventDefault();
            return;
        }
        
        $tree.tree(
            'appendNode',
            {
                label: 'New Item',
                id: newID
            },
            currentParent
        );
        
        $tree.tree('openNode', currentParent, true);
    }

    var node = $tree.tree('getNodeById', newID);
    $tree.tree('selectNode', node);
    RenameNode();

    //buildLeftNavCode();
}
function DeleteNode()
{
    var tree = $tree.tree('getTree');
    var node = $tree.tree('getSelectedNode');
    var hasNodes = false;
    $tree.tree('removeNode', node);
    
    tree.iterate(function(node) {
        hasNodes = true;
    });
    
    if (!hasNodes) AddNode();
    
    //buildLeftNavCode();
}
function MoveNode(event)
{
    if (!UniqueTopicCheck(event.move_info.target_node.id))
    {
        alert('Be sure to drag the ' + event.move_info.moved_node.name + ' element to a blank element');
        event.preventDefault();
    }
}
function RenameNode()
{
    var node = $tree.tree('getSelectedNode');
    var newName = prompt("Please enter your name", node.name);
    if (newName != null) {
        $tree.tree('updateNode', $tree.tree('getSelectedNode'), newName);
    }
    
    //$tree.tree('selectNode', node);
    SelectNode(node.id);
    //$('#txtTitle').val(newName);
    //buildLeftNavCode();
}
function SelectNode(nodeID)
{
    var tree = $tree.tree('getTree');
    var parentNode = $tree.tree('getNodeById', nodeID);
    var parentName = '';

    currentParent = parentNode;
    currentItem = null;
    
    if (parentNode.hasChildren()) {
        $tree.tree('toggle', parentNode);
    }

alert(window.location.href);

    if (parentNode != null && !HasChildren(parentNode))
    {
        if (!UniqueTopicCheck(parentNode.id))
        {
            currentItem = parentNode;
            currentID = parentNode.id;
            showTopic(parentNode.id, null);
            parentNode = parentNode.parent;
            parentName = parentNode.name;
            currentParent = parentNode;
        }
        else
        {
            if (parentNode.parent.id == null)
            {
                currentItem = null;
                currentID = null;
                parentNode = parentNode;
                parentName = parentNode.name;
                currentParent = parentNode;
            }
            else
            {
                parentName = '<a href="javascript:SelectNode(' + parentNode.parent.id + ');">' + parentNode.parent.name + '</a> &nbsp; &gt; &nbsp; ' + parentNode.name;
            }
            
            showTopic(0, null);
        }
    }
    else
    {
        parentName = parentNode.name;
    }

    $('#spnCurrentFolder').html(parentName);
}
function HasChildren(parentNode)
{
    var hasChildren = false;
    
    try
    {
        if (parentNode.hasChildren())
        {
            hasChildren = true;
        }
    }
    catch(exception) { }

    return hasChildren;
}
/* END THE TREEVIEW CODE */





















