/*

Requirements: jQuery.js

To use the ValidateFields function: 
Add a <ul> with a class that matches the value of SummaryClass
Add a <span> for each field to validate with the following custom attributes:

class -- "ErrorMessage"
isRequired -- true / false (optional - default is false)
targetControl -- the selector of the control to validate (required)
comparisonTargetControl -- the selector of the control to compare the target's value to (optional)
errorMessage -- the message to display if invalid (required)
text -- if present, this is shown in the field specific span (optional)
group -- if present, only validates controls within a validation group (the SummaryClass ul must have a matching group value)
errorClass -- if present, this sets the target field to the specified css class (optional)
dateType -- (optional, choose from one of the following)
- ssn
- date
- gender
- decimal
- decimal_formatted
- phone
- url
- email
- int
- int_nonzero
- int_formatted
- time
- zip
- racetime
- cc

(e.g.: <span class="ErrorMessage" isRequired="true" targetControl="cboReportType" errorMessage="Be sure to select the report type" errorClass="Error_Text" group="grpTestInformation" text=""></span>)

To perform validation on all the page's fields:

if (ValidateFields() == true)
{
alert('Success!');
}

*/

// global variables
var SummaryClass = 'valSumMain';

// regex variables
var Regex_SSN = /^\d{3}-\d{2}-\d{4}$/;
var Regex_Date = /(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[13-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})/;
var Regex_Gender = /^(?:m|M|male|Male|f|F|female|Female)$/;
var Regex_Decimal = /^[+-]?\d+(\.\d{1,4})? *%?$/;
var Regex_DecimalFormatted = /^(((\d{1,3})(,\d{3})*)|(\d+))(.\d+)?$/;
var Regex_Phone = /^([\(]{1}[0-9]{3}[\)]{1}[ |\-]{0,1}|^[0-9]{3}[\-| ])?[0-9]{3}(\-| ){1}[0-9]{4}$/;
var Regex_Url = /^(([http|https|HTTPS|HTTP]+:\/\/))?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([#-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/;
var Regex_Email = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/; // /^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$/; // /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
var Regex_Integer = /^\d+$/;
var Regex_IntegerFormatted = /^(\d|,)*\d*$/;
var Regex_Time = /^((([0]?[1-9]|1[0-2])(:|\.)(00|15|30|45)?( )?(AM|am|aM|Am|PM|pm|pM|Pm))|(([0]?[0-9]|1[0-9]|2[0-3])(:|\.)(00|15|30|45)?))$/;
var Regex_Zip = /^((\d{5}-\d{4})|(\d{5})|([AaBbCcEeGgHhJjKkLlMmNnPpRrSsTtVvXxYy]\d[A-Za-z]\s?\d[A-Za-z]\d))$/;
var Regex_CC = /^((4\d{3})|(5[1-5]\d{2})|(6011))-?\d{4}-?\d{4}-?\d{4}|3[4,7]\d{13}$/;
var Regex_RaceTime = /^((([0-9]?[0-9])|([2][0-3])):)?(([0-5]?[0-9]):)?([0-5][0-9])(.([0-9]?[0-9]))?$/; // /^(?:[0-9]?[0-9]:?)(?:[0-5]?[0-9]:?)(?:[0-5]?[0-9])(?:.[0-9]?[0-9])$/;



// validation methods

// Primary validation call
// group: optional validation group
//      This allows for more than one group of validators in the same page
function ValidateFields(group)
{
    var isValid = true;
    var tmp = true;
    var regex = null;
    var matches = null;

    ClearAllErrorMessages();

    $('.ErrorMessage').each(function ()
    {
        if (((group == null || group == '') && ($(this).attr('group') === undefined || $(this).attr('group') == '') || $(this).attr('group') == group))
        {
            tmp = DoValidation($(this), group);
        }
        if (tmp == false) isValid = false;
    });

    if (isValid == false) $(SummaryClass).show();

    return isValid;
}

// Iterates through the controls, calling methods that manage
// messages to the user
function DoValidation(object, group)
{
    var isValid = true;
    var prefix = '';

    if ($($(object).attr('targetControl')).length == 0) prefix = '#';

    // check the required fields
    try
    {
        if ($(object).attr('isrequired') == 'true')
        {
            if ($(prefix + $(object).attr('targetControl')).prop('tagName').toLowerCase() == 'select')
            {
                if ($(prefix + $(object).attr('targetControl')).val().length < 1 ||
                    $(prefix + $(object).attr('targetControl')).val() == '0' ||
                    $(prefix + $(object).attr('targetControl')).val() == '-1')
                {
                    AddErrorMessage(object, group);
                    isValid = false;
                }
            }
            else
            {
                if ($(prefix + $(object).attr('targetControl')).val().length < 1)
                {
                    AddErrorMessage(object, group);
                    isValid = false;
                }
            }
        }
    }
    catch (ex)
    {
        alert(ex + ' target control: ' + $(object).attr('targetControl'));
    }

    // check the comparison fields
    try
    {
        if ($(object).attr('comparisonTargetControl') != null && $(object).attr('comparisonTargetControl') !== undefined)
        {
            if (($(prefix + $(object).attr('targetControl')).val() != $(prefix + $(object).attr('comparisonTargetControl')).val()) && $(prefix + $(object).attr('comparisonTargetControl')).val().length > 0)
            {
                AddErrorMessage(object, group);
                isValid = false;
            }
        }
    }
    catch (ex)
    {
        alert(ex);
    }

    // check the data types
    if ($(object).attr('dataType') != '' && $(object).attr('dataType') !== undefined)
    {
        if ($(object).attr('dataType') == 'ssn') regex = Regex_SSN;
        if ($(object).attr('dataType') == 'date') regex = Regex_Date;
        if ($(object).attr('dataType') == 'gender') regex = Regex_Gender;
        if ($(object).attr('dataType') == 'decimal') regex = Regex_Decimal;
        if ($(object).attr('dataType') == 'decimal_formatted') regex = Regex_DecimalFormatted;
        if ($(object).attr('dataType') == 'phone') regex = Regex_Phone;
        if ($(object).attr('dataType') == 'url') regex = Regex_Url;
        if ($(object).attr('dataType') == 'email') regex = Regex_Email;
        if ($(object).attr('dataType') == 'int') regex = Regex_Integer;
        if ($(object).attr('dataType') == 'int_nonzero') regex = Regex_Integer;
        if ($(object).attr('dataType') == 'int_formatted') regex = Regex_IntegerFormatted;
        if ($(object).attr('dataType') == 'time') regex = Regex_Time;
        if ($(object).attr('dataType') == 'zip') regex = Regex_Zip;
        if ($(object).attr('dataType') == 'racetime') regex = Regex_RaceTime;
        if ($(object).attr('dataType') == 'cc') regex = Regex_CC;

        try
        {
            if (regex != null)
            {
                if ($(prefix + $(object).attr('targetControl')).val().length > 0)
                {
                    matches = ($(prefix + $(object).attr('targetControl')).val()).match(regex);
                    if (!matches)
                    {
                        AddErrorMessage(object, group);
                        isValid = false;
                    }
                    else
                    {
                        // be sure the nonzero integer is greater than 0
                        if ($(object).attr('dataType') == 'int_nonzero' && parseInt($(prefix + $(object).attr('targetControl')).val()) < 1)
                        {
                            AddErrorMessage(object, group);
                            isValid = false;
                        }
                    }
                }
            }
        }
        catch (ex) { alert('The following targetControl value is invalid (verify that this id is correct): ' + $(object).attr('targetControl')) }
    }

    return isValid;
}

// Allows for custom error messages to be displayed
function AddCustomErrorMessage(message, group)
{
    $('.' + SummaryClass).each(function ()
    {
        if (((group == null || group == '') && ($(this).attr('group') === undefined || $(this).attr('group') == '') || $(this).attr('group') == group))
        {
            $(this).append('<li id="liSummary_cust"> ' + message + '</li>');
        }
    });
}

// Adds the specified control's error message to the summary
function AddErrorMessage(object, group)
{
    var summary = null;
    var targetID = $(object).attr('targetControl');

    var prefix = '';
    if ($(targetID) == null) prefix = '#';

    // do the error message adding
    if ($(object).attr("text") != null)
        $(object).html($(object).attr("text"));
    else
        $(object).html($(object).attr("errorMessage"));

    if ($(object).attr("errorClass") != null && $(object).attr("errorClass") !== undefined)
    {
        $(prefix + targetID).addClass($(object).attr("errorClass"));
    }

    $('.' + SummaryClass).each(function ()
    {
        if (((group == null || group == '') && ($(this).attr('group') === undefined || $(this).attr('group') == '') || $(this).attr('group') == group))
        {
            $(this).append('<li id="liSummary_' + $(object).attr('targetControl').replace('#', '') + '"> ' + $(object).attr("errorMessage") + '</li>');
        }
    });
}

// Removes the specified control's error message from the summary
function ClearErrorMessage(object)
{
    var prefix = '';
    if ($($(object).attr('targetControl')).length == 0) prefix = '#';

    if ($(object).attr("errorClass") != null) $(prefix + $(object).attr('targetControl')).removeClass($(object).attr("errorClass"));
    $('#liSummary_' + $(object).attr('targetControl').replace('#', '')).remove();
    $(object).html('');
}

// Removes all error messages from the summary (within the group)
function ClearAllErrorMessages(group)
{
    var prefix = '';

    $('.' + SummaryClass).html('');
    $('.ErrorMessage').html('');

    // clear out the error classes if any
    $('.ErrorMessage').each(function ()
    {
        if ($($(this).attr('targetControl')).length == 0) prefix = '#';

        //if (((group == null || group == '') && ($(this).attr('group') === undefined || $(this).attr('group') == '') || $(this).attr('group') == group))
        if (((group == null || group == '') || $(this).attr('group') == group))
        {
            $(prefix + $(this).attr('targetControl')).removeClass($(this).attr("errorClass"));
        }
    });
}

// Removes the error class from all the controls (within the group)
function ClearAllErrorClass(errorClass, group)
{
    $('.' + errorClass).removeClass(errorClass);
}



// -----------------------------------------------------------------------------------------------------
/*

To implement the on the fly validation, add the following to the page:

UseRealTimeValidation(group)

the group parameter is optional

*/
function UseRealTimeValidation(group)
{
    var prefix = '';
    var prefix2 = '';

    $('.ErrorMessage').each(function ()
    {
        if ($($(this).attr('targetControl')).length == 0) prefix = '#';
        if ($($(this).attr('comparisonTargetControl')) == null) prefix2 = '#';

        $(prefix + $(this).attr('targetControl')).blur(function () { ValidateField($(this), group); });
        $(prefix2 + $(this).attr('comparisonTargetControl')).blur(function () { ValidateField($(this), group); });
    });
}

function ValidateField(field, group)
{
    if (field == null) return;

    var prefix = '';
    var prefix2 = '';

    $('.ErrorMessage').each(function ()
    {
        if ($($(this).attr('targetControl')).length == 0) prefix = '#';
        if ($($(this).attr('comparisonTargetControl')) == null) prefix2 = '#';

        try
        {
            if ($(this).attr('targetControl').replace(prefix, '') == $(field).attr('id') ||
                $(this).attr('comparisonTargetControl').replace(prefix2, '') == $(field).attr('id'))
            {
                if (((group == null || group == '') && ($(this).attr('group') === undefined || $(this).attr('group') == '') || $(this).attr('group') == group))
                {
                    ClearErrorMessage($(this));
                    DoValidation($(this), group);
                }
            }
        }
        catch (ex) { }
    });
}





// -----------------------------------------------------------------------------------------------------
/*

General helper functions

*/
function ClearFields(parentSelector)
{
    try
    {
        $(parentSelector).find("input[type=text], textarea, select").val("");
    } catch(ex) { }
    
    try
    {
        for (edId in tinyMCE.editors) tinymce.get(edId).setContent('');
    } catch(ex) { }

    $(parentSelector).find("input[type=checkbox]").attr('checked', false);
    $(parentSelector).find("select").selectedIndex = '0';
}

function SelectFirstTextBox(parentSelector)
{
    if (parentSelector + '' == '') $('input[type=text]:enabled:first').focus();
    else $(parentSelector).first().find('input[type=text]:enabled:first').focus();
}

/*

    EXAMPLE:

    var arr = new Array();
    var ix = 0;
    var url = '/Grids/CoachSearch';
    var target = '#pnlSearchResults';

    arr[ix] = 'name:' + $('#txtName').val();ix++;
    arr[ix] = 'teamName:' + $('#txtTeamName').val();ix++;
    arr[ix] = 'collegeDivisionID:' + $('#cboCollegeDivision').val();ix++;
    if ($('#cboCollegeSubDivision').val() !== undefined) arr[ix] = 'collegeSubDivisionID:' + $('#cboCollegeSubDivision').val();ix++;

    LoadView(BuildQuery(url, arr), target);

*/
function BuildQuery(baseUrl, fieldsArray)
{
    var qry = '';
    var tmp = null;

    $.each(fieldsArray, function (key, value)
    {
        try
        {
            tmp = value.split(':');

            if (qry.indexOf('?') < 0)
            {
                qry = qry += '?';
            }
            else
            {
                qry = qry += '&';
            }
            qry = qry += tmp[0] + '=' + EncodeUrl(tmp[1]);
        } catch (ex) { }
    });

    return baseUrl + qry;
}

function SubmitForm(action, formSelector)
{
    var frm = $('form').get(0);

    if (formSelector != null) frm = $(formSelector);



    $(frm).attr('action', action);
    $(frm).submit();
}

// To use this:
// SubmitFormToTarget('/Team/UpdateGoal', 'ifrmSaveGoalFinish', '#formEditGoal');
function SubmitFormToTarget(action, target, formSelector)
{
    var frm = $('form').get(0);

    if (formSelector != null) frm = $(formSelector);

    $(frm).attr('action', action);
    $(frm).attr('target', target);

    $(frm).submit();
}

// encode and decode urls
function EncodeUrl(url)
{
    return encodeURIComponent(url);
}
function DecodeUrl(url)
{
    return decodeURIComponent(url);
}

// check that a date is in the future
function IsCurrentDate(dateIn)
{
    var _now = new Date();
    var _date = new Date(dateIn);
    var isFuture = false;

    if (_date.getTime() >= _now.getTime())
    {
        isFuture = true;
    }

    return isFuture;
}

// get age from birthdate
function GetAge(birthday)
{
    birthday = new Date(birthday);
    var today = new Date();
    var thisYear = 0;
    if (today.getMonth() < birthday.getMonth())
    {
        thisYear = 1;
    } else if ((today.getMonth() == birthday.getMonth()) && today.getDate() < birthday.getDate())
    {
        thisYear = 1;
    }
    var age = today.getFullYear() - birthday.getFullYear() - thisYear;
    return age;
}

// load a page into a specified object
function LoadView(sourceUrl, targetControlSelector)
{
    var ajx;

    if (ajx !== undefined) { ajx.abort(); }
    ajx = $.ajax({
        url: sourceUrl,
        success: function (data)
        {
            $(targetControlSelector).html(data);
            ajx = null;
        }
    });
}

// to use:  if (getQueryValue()['test'] == 'good') ...
function getQueryValue()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}


// to use:
// create the EnterCommand function with a commandName and optionally commandValue that will allow you to
// determine the action being taken
function KeyCheck(controlSelector, commandName, commandValue)
{
    $(controlSelector).on('keyup', function (e)
    {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13)
        {
            EnterCommand(commandName, commandValue);
            return false;
        }
    });
}



// show a jq dialog
function ShowDialog(title, url, targetSelector, dialogHeight, dialogWidth, isModal)
{
    $(targetSelector).html('');

    // show a match dialog
    LoadView(url, targetSelector);
    $(targetSelector).dialog({
        title: title,
        height: dialogHeight,
        modal: isModal,
        width: dialogWidth
    });
    // hide the dialog close button text
    $('.ui-icon-closethick').html('');
}


// Show ghosted default text
/*
Add this class to the stylesheet:

.GhostText { color:#999 !important; }
*/
function SetGhostedDefaultText(elementSelector)
{
    $(elementSelector).addClass("GhostText");

    $(elementSelector).blur(function (e)
    {
        if ($.trim($(this).val()) == "")
        {
            $(this).val(e.target.defaultValue);
            $(this).addClass("GhostText");
        }
    });
    $(elementSelector).focus(function (e)
    {
        if ($.trim($(this).val()) == e.target.defaultValue)
        {
            $(this).val('');
            $(this).removeClass("GhostText");
        }
    });
}
