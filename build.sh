#!/bin/sh
javac -cp .:release/lib/* -d ./compiled ./code/*.java
cd compiled
jar -cfm ../release/jjhelp.jar manifest.txt *.class
